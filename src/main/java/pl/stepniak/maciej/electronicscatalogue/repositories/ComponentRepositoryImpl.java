package pl.stepniak.maciej.electronicscatalogue.repositories;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.log4j.Logger;
import pl.stepniak.maciej.electronicscatalogue.exceptions.UnknownComponentIdException;
import pl.stepniak.maciej.electronicscatalogue.models.Component;

/**
 * Klasa implementująca niestandardowe metody zapewniające dostęp do modelu
 * danych typu Component i klas pochodnych. Dodatkowo dostępne są Query Methods.
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public class ComponentRepositoryImpl implements ComponentCustomMethodsRepository {

    Logger logger = Logger.getLogger(this.getClass());

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public long getComponentsAmountInCategory(String className) {
        StringBuilder namedQuery = new StringBuilder(className);
        namedQuery.append(".countAll");
        Query query = entityManager.createNamedQuery(namedQuery.toString());  // np. "Resistor.countAll"
        return (long) query.getSingleResult(); // odpytujemy o liczbę elementów, więc będzie rezultat, ew. równy 0
    }

    @Override
    public List<? extends Component> getComponentsInCategory(String className, int limit, int offset) {
        StringBuilder namedQuery = new StringBuilder(className);
        namedQuery.append(".findAll");
        Query query = entityManager.createNamedQuery(namedQuery.toString());  // np. "Resistor.findAll"
        query.setMaxResults(limit);
        query.setFirstResult(offset);
        return query.getResultList();
    }

    @Override
    public Component getComponentById(int id, Class clazz) {
        String className = clazz.getSimpleName();
        StringBuilder namedQuery = new StringBuilder(className);
        namedQuery.append(".findById");

        Query query = entityManager.createNamedQuery(namedQuery.toString());  // np. "Resistor.findById"
        query.setParameter("id", (long) id);

        Component component = null;
        try {
            component = (Component) query.getSingleResult();
        } catch (NoResultException | NonUniqueResultException ex) {
            throw new UnknownComponentIdException();
        }
        return component;
    }

    @Override
    public void addComponent(Component component) {
        entityManager.persist(component);
    }

    @Override
    public void updateComponent(Component component) {
        entityManager.merge(component);
    }

    @Override
    public void deleteComponent(Component component) {
        //entityManager.remove(component); // odkomentować, żeby usunąć z bazy
    }
}
