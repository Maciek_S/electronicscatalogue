package pl.stepniak.maciej.electronicscatalogue.repositories;

import java.util.List;
import pl.stepniak.maciej.electronicscatalogue.models.Component;

/**
 * Interfejs deklarujący niestandardowe funkcje dostępne w warstwie dostępu do
 * danych dla obiektów typu Component i pochodnych.
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public interface ComponentCustomMethodsRepository {

    public Component getComponentById(int id, Class clazz);

    public long getComponentsAmountInCategory(String className);

    public List<? extends Component> getComponentsInCategory(String className, int limit, int offset);

    public void addComponent(Component component);

    public void updateComponent(Component component);

    public void deleteComponent(Component component);
}
