package pl.stepniak.maciej.electronicscatalogue.repositories;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.log4j.Logger;
import pl.stepniak.maciej.electronicscatalogue.models.User;
import pl.stepniak.maciej.electronicscatalogue.models.UserRole;

/**
 * Klasa implementująca niestandardowe metody zapewniające dostęp do modelu
 * danych typu User. Dodatkowo dostępne są Query Methods.
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public class UserRepositoryImpl implements UserCustomMethodsRepository {

    Logger logger = Logger.getLogger(this.getClass());

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public void saveRole(UserRole role) {
        entityManager.persist(role);
    }

    @Override
    public void deleteRole(UserRole role) {
        // sprawdź czy UserRole jest zarządzane w aktualnej transakcji, jeśli nie zarządzaj rolą
        entityManager.remove(entityManager.contains(role) ? role : entityManager.merge(role));
    }

    @Override
    public void updateUser(User user) {
        entityManager.merge(user);
    }

}
