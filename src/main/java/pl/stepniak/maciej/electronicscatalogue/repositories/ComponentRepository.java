package pl.stepniak.maciej.electronicscatalogue.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.stepniak.maciej.electronicscatalogue.models.Component;

/**
 * Interfejs umożliwiający korzystanie ze metod Spring Data oraz własnych
 * funkcji dla obiektów typu Component i pochodnych.
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
@Repository
public interface ComponentRepository extends CrudRepository<Component, Long>, ComponentCustomMethodsRepository {

}
