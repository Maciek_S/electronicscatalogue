package pl.stepniak.maciej.electronicscatalogue.repositories;

import pl.stepniak.maciej.electronicscatalogue.models.User;
import pl.stepniak.maciej.electronicscatalogue.models.UserRole;

/**
 * Interfejs deklarujący niestandardowe funkcje dostępne w warstwie dostępu do
 * danych dla obiektów typu User.
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public interface UserCustomMethodsRepository {

    public void saveRole(UserRole role);

    public void deleteRole(UserRole role);

    public void updateUser(User user);
}
