package pl.stepniak.maciej.electronicscatalogue.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.stepniak.maciej.electronicscatalogue.models.User;

/**
 * Interfejs umożliwiający korzystanie ze metod Spring Data oraz własnych
 * funkcji dla obiektów typu User.
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
@Repository
public interface UserRepository extends CrudRepository<User, String>, UserCustomMethodsRepository {

}
