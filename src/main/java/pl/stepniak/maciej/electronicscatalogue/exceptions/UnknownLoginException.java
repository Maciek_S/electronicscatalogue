package pl.stepniak.maciej.electronicscatalogue.exceptions;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public class UnknownLoginException extends NullPointerException {

    public static final String message = "Nie znaleziono użytkownika o podanej nazwie.";

    public UnknownLoginException() {
        super(message);
    }
}
