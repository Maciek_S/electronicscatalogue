package pl.stepniak.maciej.electronicscatalogue.exceptions;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public class OldPasswordMismatchException extends RuntimeException {

    public static final String message = "Wprowadzone stare hasło jest niepoprawne.";

    public OldPasswordMismatchException() {
        super(message);
    }
}
