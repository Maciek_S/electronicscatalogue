package pl.stepniak.maciej.electronicscatalogue.exceptions;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public class NoSubcategoryFoundException extends RuntimeException {

    public static final String message = "Nie znaleziono podkategorii. Zgłoś problem do administratora systemu.";

    public NoSubcategoryFoundException() {
        super(message);
    }

}
