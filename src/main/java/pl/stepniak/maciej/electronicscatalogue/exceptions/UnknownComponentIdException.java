package pl.stepniak.maciej.electronicscatalogue.exceptions;

import javax.persistence.NoResultException;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public class UnknownComponentIdException extends NoResultException {

    public static final String message = "Komponent o podanym ID nie istnieje w bazie danych.";

    public UnknownComponentIdException() {
        super(message);
    }
}
