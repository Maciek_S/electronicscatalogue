package pl.stepniak.maciej.electronicscatalogue.exceptions;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public class ToSmallComponentsInStockException extends IllegalArgumentException {

    public static final String message = "Zbyt mało elementów w magazynie.";

    public ToSmallComponentsInStockException() {
        super(message);
    }
}
