package pl.stepniak.maciej.electronicscatalogue.exceptions;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public class UnknownRoleException extends IllegalArgumentException {

    public static final String message = "Podana rola jest nieprawidłowa.";

    public UnknownRoleException() {
        super(message);
    }
}
