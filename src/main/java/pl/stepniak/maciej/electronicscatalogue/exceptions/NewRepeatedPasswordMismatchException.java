package pl.stepniak.maciej.electronicscatalogue.exceptions;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public class NewRepeatedPasswordMismatchException extends RuntimeException {

    public static final String message = "Powtórnie wprowadzone hasło różni się od pierwotnego.";

    public NewRepeatedPasswordMismatchException() {
        super(message);
    }
}
