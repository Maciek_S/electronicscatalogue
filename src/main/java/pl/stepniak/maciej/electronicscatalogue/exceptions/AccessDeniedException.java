package pl.stepniak.maciej.electronicscatalogue.exceptions;

/**
 * Klasa wyjątku. Brak uprawnień do przeglądania tego zasobu..
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public class AccessDeniedException extends RuntimeException {

    public static final String message = "Brak uprawnień do przeglądania tego zasobu.";

    public AccessDeniedException() {
        super();
    }

}
