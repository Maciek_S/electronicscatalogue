package pl.stepniak.maciej.electronicscatalogue.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * Klasa wyjątku. Nie można znaleźć żądanego zasobu.
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public class PageNotFoundException extends NoHandlerFoundException {

    public static final String message = "Strona którą próbujesz wyświetlić nie istnieje.";

    public PageNotFoundException(String httpMethod, String requestURL, HttpHeaders headers) {
        super(httpMethod, requestURL, headers);
    }
}
