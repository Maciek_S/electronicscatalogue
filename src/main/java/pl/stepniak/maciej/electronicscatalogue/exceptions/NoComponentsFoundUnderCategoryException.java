package pl.stepniak.maciej.electronicscatalogue.exceptions;

/**
 * Klasa wyjątku. Brak komponentów we wskazanej kategorii.
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public class NoComponentsFoundUnderCategoryException extends RuntimeException {

    public static final String message = "Brak komponentów we wskazanej kategorii.";

    public NoComponentsFoundUnderCategoryException() {
        super(message);
    }
}
