package pl.stepniak.maciej.electronicscatalogue.exceptions;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public class ParameterNotPresentException extends RuntimeException {

    public static final String message = "Nie podano wymaganego parametru.";

    public ParameterNotPresentException() {
        super(message);
    }
}
