package pl.stepniak.maciej.electronicscatalogue.models;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public enum CapacityUnitEnum implements UnitEnum {

    PF("pF"), NF("nF"), UF("uF");
    private String label;

    private CapacityUnitEnum(String label) {
        this.label = label;
    }

    public String getLabel() {
        return this.label;
    }

    public static CapacityUnitEnum getInstance(String unit) {
        for (CapacityUnitEnum item : CapacityUnitEnum.values()) {
            if (item.getLabel().equals(unit)) {
                return item;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return getLabel();
    }
}
