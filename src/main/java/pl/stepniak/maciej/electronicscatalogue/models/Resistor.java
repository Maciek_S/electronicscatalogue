package pl.stepniak.maciej.electronicscatalogue.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.SafeHtml;
import pl.stepniak.maciej.electronicscatalogue.annotations.DimensionFormatXYZ;
import pl.stepniak.maciej.electronicscatalogue.annotations.FloatMinGreaterThan;
import pl.stepniak.maciej.electronicscatalogue.annotations.FloatRange;
import pl.stepniak.maciej.electronicscatalogue.models.subcategoriesEnums.PassiveComponentsCategoriesENUM;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
@Entity
@Table(name = "resistors")
@NamedQueries({
    @NamedQuery(name = "Resistor.findAll", query = "SELECT r FROM Resistor r"),
    @NamedQuery(name = "Resistor.findById", query = "SELECT r FROM Resistor r WHERE r.id = :id"),
    @NamedQuery(name = "Resistor.findByNominalValue", query = "SELECT r FROM Resistor r WHERE r.nominalValue = :value"),
    @NamedQuery(name = "Resistor.countAll", query = "SELECT COUNT(r) FROM Resistor r")
})
public class Resistor extends PassiveComponent {

    @Setter
    @Getter
    @FloatMinGreaterThan(min = 0, message = "{resistor.nominalValue.floatMinGreaterThan}")
    @Column
    float nominalValue;

    @Getter
    @Setter
    @NotNull
    @Enumerated(EnumType.STRING)
    ResistanceUnitEnum nominalValueUnit;

    @Setter
    @Getter
    @FloatRange(min = 0.1F, max = 10, message = "{resistor.tolerance.floatRange}")
    float tolerance; // [%]

    @Setter
    @Getter
    @FloatMinGreaterThan(min = 0, message = "{resistor.powerRating.floatMinGreaterThan}")
    float powerRating; // [W]

    @Setter
    @Getter
    @FloatMinGreaterThan(min = 0, message = "{resistor.maxVoltage.floatMinGreaterThan}")
    float maxVoltage; // [V]

    @Setter
    @Getter
    @DimensionFormatXYZ(message = "{general.dimensionFormatXYZ}")
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE, message = "{general.safeHtml}")
    String dimensions;

    public Resistor() {
        super();
        this.label = "Rezystor";
        this.nominalValueUnit = ResistanceUnitEnum.OHM;
        this.nominalValue = 0;
        this.tolerance = 0;
        this.powerRating = 0;
        this.maxVoltage = 0;
        this.unitsInStock = 0;
        this.subcategory = PassiveComponentsCategoriesENUM.RESISTORS;
        this.mainCategory = MainCategoryENUM.PASSIVE_COMPONENTS;
    }

    public String getDescription() {
        StringBuilder toReturn = new StringBuilder(String.valueOf(this.nominalValue));
        toReturn.append(" ").append(this.nominalValueUnit.getLabel());
        toReturn.append("; ");
        toReturn.append(String.valueOf(tolerance)).append("%");
        toReturn.append("; ");
        toReturn.append(String.valueOf(this.powerRating)).append(" W");
        toReturn.append("; ");
        toReturn.append(String.valueOf(this.maxVoltage)).append(" V");
        toReturn.append("; ");

        return toReturn.toString();
    }

    @Override
    public String toString() {
        return "Resistor{" + "nominalValue=" + nominalValue + ", nominalValueUnit=" + nominalValueUnit + ", tolerance=" + tolerance
                + ", powerRating=" + powerRating + ", maxVoltage=" + maxVoltage + ", unitInStock=" + unitsInStock
                + ", id=" + id
                + "}";
    }
}
