package pl.stepniak.maciej.electronicscatalogue.models.subcategoriesEnums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public enum OptoCategoriesENUM implements SubcategoriesEnum {

    // Elementy pasywne
    LED_DIODES("Diody LED"),
    DISPLAYS("Wyświetlacze");
//    LIGHT_SOURCES("Źródła światła"),
//    TRANSOPTORS("Transoptory"),
//    PHOTOELEMENTS("Fotoelementy");

    private final String label;

    private OptoCategoriesENUM(String label) {
        this.label = label;
    }

    @Override
    public String getLabel() {
        return this.label;
    }

    public static List<String> getLabels() {
        List<String> labels = new ArrayList<>();
        for (OptoCategoriesENUM item : OptoCategoriesENUM.values()) {
            labels.add(item.getLabel());
        }
        return labels;
    }

    public static OptoCategoriesENUM getInstance(String name) {
        for (OptoCategoriesENUM item : OptoCategoriesENUM.values()) {
            if (item.getLabel().equals(name)) {
                return item;
            }
        }
        return null;
    }

    public static List<?> valuesList() {
        List<OptoCategoriesENUM> list = new ArrayList<>();
        list.addAll(Arrays.asList(OptoCategoriesENUM.values()));
        return list;
    }

}
