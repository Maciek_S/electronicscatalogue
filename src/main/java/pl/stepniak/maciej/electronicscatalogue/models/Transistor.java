package pl.stepniak.maciej.electronicscatalogue.models;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import pl.stepniak.maciej.electronicscatalogue.models.subcategoriesEnums.SemiconductorsCategoriesENUM;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
@Entity
@Table(name = "transistors")
@NamedQueries({
    @NamedQuery(name = "Transistor.findAll", query = "SELECT t FROM Transistor t"),
    @NamedQuery(name = "Transistor.findById", query = "SELECT t FROM Transistor t WHERE t.id = :id"),
    @NamedQuery(name = "Transistor.findByType", query = "SELECT t FROM Transistor t WHERE t.transistorType = :type"),
    @NamedQuery(name = "Transistor.countAll", query = "SELECT COUNT(t) FROM Transistor t")
})
public class Transistor extends Semiconductor {

    @Getter
    @Setter
    @NotNull
    @Enumerated(EnumType.STRING)
    TransistorTypeENUM transistorType;

    @Setter
    @Getter
    @SafeHtml(whitelistType = WhiteListType.NONE, message = "{general.safeHtml}")
    @Length(min = 1, max = 50, message = "{transistor.manufacturer.length}")
    String manufacturer;

    @Setter
    @Getter
    @Length(min = 1, max = 10, message = "{transistor.symbol.length}")
    @SafeHtml(whitelistType = WhiteListType.NONE, message = "{general.safeHtml}")
    String symbol;

    @Setter
    @Getter
    @NotNull
    @Enumerated(EnumType.STRING)
    TransistorCaseTypeENUM transistorCaseType;

    public Transistor() {
        super();
        this.label = "Tranzystor";
        this.unitsInStock = 0;
        this.transistorCaseType = TransistorCaseTypeENUM.TO92;
        this.subcategory = SemiconductorsCategoriesENUM.TRANSISTORS;
        this.mainCategory = MainCategoryENUM.SEMICONDUCTOR_COMPONENTS;
    }

    public String getDescription() {
        StringBuilder toReturn = new StringBuilder("");
        toReturn.append(" ").append(transistorType.getLabel());
        toReturn.append(" ").append(symbol);
        toReturn.append(" ").append(transistorCaseType.toString());

        return toReturn.toString();
    }

    @Override
    public String toString() {
        return getDescription();
    }
}
