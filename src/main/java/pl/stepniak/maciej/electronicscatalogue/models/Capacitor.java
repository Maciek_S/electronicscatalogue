package pl.stepniak.maciej.electronicscatalogue.models;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import pl.stepniak.maciej.electronicscatalogue.annotations.FloatMinGreaterThan;
import pl.stepniak.maciej.electronicscatalogue.annotations.FloatRange;
import pl.stepniak.maciej.electronicscatalogue.models.subcategoriesEnums.PassiveComponentsCategoriesENUM;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
@Entity
@Table(name = "capacitors")
@NamedQueries({
    @NamedQuery(name = "Capacitor.findAll", query = "SELECT c FROM Capacitor c"),
    @NamedQuery(name = "Capacitor.findById", query = "SELECT c FROM Capacitor c WHERE c.id = :id"),
    @NamedQuery(name = "Capacitor.findByNominalValue", query = "SELECT c FROM Capacitor c WHERE c.nominalValue = :value"),
    @NamedQuery(name = "Capacitor.countAll", query = "SELECT COUNT(c) FROM Capacitor c")
})
public class Capacitor extends PassiveComponent {

    @Getter
    @FloatMinGreaterThan(min = 0, message = "{capacitor.nominalValue.floatRange}")
    float nominalValue;

    @Getter
    @NotNull
    @Enumerated(EnumType.STRING)
    CapacityUnitEnum nominalValueUnit;

    @Setter
    @Getter
    @FloatRange(min = 0.1F, max = 10, message = "{capacitor.tolerance.floatRange}")
    float tolerance; // [%]

    public Capacitor() {
        super();
        this.label = "Kondensator";
        this.nominalValueUnit = CapacityUnitEnum.UF;
        this.nominalValue = 0;
        this.tolerance = 0;
        this.unitsInStock = 0;
        this.subcategory = PassiveComponentsCategoriesENUM.CAPACITORS;
        this.mainCategory = MainCategoryENUM.PASSIVE_COMPONENTS;
    }

    public void setNominalValueUnit(CapacityUnitEnum nominalValueUnit) {
        this.nominalValueUnit = nominalValueUnit;
    }

    public void setNominalValue(float nominalValue) {
        this.nominalValue = nominalValue;
    }

    public String getDescription() {
        StringBuilder toReturn = new StringBuilder(String.valueOf(this.nominalValue));
        toReturn.append(" ").append(this.nominalValueUnit.getLabel());
        toReturn.append("; ");
        toReturn.append(String.valueOf(tolerance)).append("%");
        toReturn.append("; ");

        return toReturn.toString();
    }

    @Override
    public String toString() {
        return getDescription();
    }
}
