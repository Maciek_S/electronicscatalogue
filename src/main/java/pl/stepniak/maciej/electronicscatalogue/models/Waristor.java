package pl.stepniak.maciej.electronicscatalogue.models;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.SafeHtml;
import pl.stepniak.maciej.electronicscatalogue.annotations.DimensionFormatXYZ;
import pl.stepniak.maciej.electronicscatalogue.annotations.FloatMinGreaterThan;
import pl.stepniak.maciej.electronicscatalogue.models.subcategoriesEnums.PassiveComponentsCategoriesENUM;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
@Entity
@Table(name = "waristors")
@NamedQueries({
    @NamedQuery(name = "Waristor.findAll", query = "SELECT w FROM Waristor w"),
    @NamedQuery(name = "Waristor.findById", query = "SELECT w FROM Waristor w WHERE w.id = :id"),
    @NamedQuery(name = "Waristor.findByVoltage", query = "SELECT w FROM Waristor w WHERE w.voltage = :voltage"),
    @NamedQuery(name = "Waristor.countAll", query = "SELECT COUNT(w) FROM Waristor w")
})
public class Waristor extends PassiveComponent {

    @Getter
    @Setter
    @NotNull
    @Enumerated(EnumType.STRING)
    AssemblingTechnologyEnum assembly;

    @Setter
    @Getter
    @FloatMinGreaterThan(min = 0, message = "{waristor.voltage.floatMinGreaterThan}")
    float voltage; // [V]

    @Setter
    @Getter
    @FloatMinGreaterThan(min = 0, message = "{waristor.maxACVoltage.floatMinGreaterThan}")
    float maxACVoltage; // [V]

    @Setter
    @Getter
    @FloatMinGreaterThan(min = 0, message = "{waristor.maxDCVoltage.floatMinGreaterThan}")
    float maxDCVoltage; // [V]

    @Setter
    @Getter
    @FloatMinGreaterThan(min = 0, message = "{waristor.maxCurrent.floatMinGreaterThan}")
    float maxCurrent; //[A]

    @Setter
    @Getter
    @FloatMinGreaterThan(min = 0.001F, message = "{waristor.power.floatMinGreaterThan}")
    float power; //[mW]

    @Setter
    @Getter
    @FloatMinGreaterThan(min = 0.01F, message = "{waristor.energy.floatMinGreaterThan}")
    float energy; // [J]

    @Setter
    @Getter
    @DimensionFormatXYZ(message = "{general.dimensionFormatXYZ}")
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE, message = "{general.safeHtml}")
    String dimensions;

    public Waristor() {
        super();
        this.label = "Warystor";
        this.unitsInStock = 0;
        this.assembly = AssemblingTechnologyEnum.SMD;
        this.subcategory = PassiveComponentsCategoriesENUM.WARISTORS;
        this.mainCategory = MainCategoryENUM.PASSIVE_COMPONENTS;
    }

    public String getDescription() {
        StringBuilder toReturn = new StringBuilder(assembly.toString());
        toReturn.append("; ").append(String.valueOf(maxACVoltage)).append(" VAC/");
        toReturn.append(String.valueOf(maxDCVoltage)).append(" VDC");
        toReturn.append("; ");
        toReturn.append(String.valueOf(maxCurrent)).append("A");
        toReturn.append("; ");
        toReturn.append(String.valueOf(power)).append(" mW");
        toReturn.append("; ");
        toReturn.append(String.valueOf(energy)).append(" J");

        return toReturn.toString();
    }

    @Override
    public String toString() {
        return getDescription();
    }

}
