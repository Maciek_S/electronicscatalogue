package pl.stepniak.maciej.electronicscatalogue.models;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import lombok.Data;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.SafeHtml;
import pl.stepniak.maciej.electronicscatalogue.annotations.MobilePhonePLFormat;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
@Entity
@Data
@Table(name = "users")
@NamedQueries({
    @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
    @NamedQuery(name = "User.findByNameAndSurname", query = "SELECT u FROM User u WHERE u.name = :name AND u.surname = :surname"),
    @NamedQuery(name = "User.findByLogin", query = "SELECT u FROM User u WHERE u.username = :login")
})
public class User implements Serializable {

    @Id
    @Column(name = "username", nullable = false, unique = true)
    @Size(min = 2, max = 50, message = "{user.username.size}")
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE, message = "{general.safeHtml}")
    private String username;

    @Column(name = "password", columnDefinition = "VARCHAR(150)")
    // @Size(min = 2, max = 50, message = "{user.password.size}") // walidacja nie działa bo jak hashujemy to hasło się wydłuża niż to wpisane
    // TODO: walidacja dla pola hasło
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE, message = "{general.safeHtml}")
    private String password;

    @Column(name = "name", nullable = false)
    @Size(min = 2, max = 50, message = "{user.name.size}")
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE, message = "{general.safeHtml}")
    private String name;

    @Column(name = "surname", nullable = false)
    @Size(min = 2, max = 50, message = "{user.surname.size}")
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE, message = "{general.safeHtml}")
    private String surname;

    @Column(name = "email")
    @Email(message = "{user.email.format}")
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE, message = "{general.safeHtml}")
    private String email;

    @Column(name = "mobile")
    @MobilePhonePLFormat(message = "{user.mobile.format}")
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE, message = "{general.safeHtml}")
    private String mobile;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    private Set<UserRole> userRoles = new HashSet<>();

    @Column(name = "enabled", columnDefinition = "tinyint default 1")
    @NotNull
    private int enabled;

    @Column(name = "datepasschange")
    @Past(message = "Data nie może być z przyszłości.")
    private java.sql.Date datepasschange;
//    @Column(name = "photo")
//    private MultipartFile photo;
}
