package pl.stepniak.maciej.electronicscatalogue.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public enum IntegratedCircuitCaseTypeENUM {

    DDPAK("DDPAK"),
    DPAK("DPAK"),
    DIP("DIP"),
    SQP("SQP"),
    SW("SW"),
    FDIP("FDIP"),
    PDIP("PDIP"),
    PENTAWATT("PENTAWATT"),
    TO220("TO220"),
    TO2205("TO2205"),
    TO220ISO("TO220ISO"),
    PLCC("PLCC"),
    QDIP("QDIP"),
    QFP("QFP"),
    TO252("TO252"),
    TO263("TO263"),
    TO268("TO268"),
    SIP("SIP"),
    SO("SO"),
    SO8("SO8"),
    TO3("TO3"),
    TO52("TO52"),
    TO99("TO99"),
    SOT223("SOT223"),
    SOT23("SOT23"),
    SQL("SQL"),
    TSOP("TSOP"),
    ZIP("ZIP");

    private String label;

    private IntegratedCircuitCaseTypeENUM(String label) {
        this.label = label;
    }

    public String getLabel() {
        return this.label;
    }

    public static List<String> getLabels() {
        List<String> labels = new ArrayList<>();
        for (IntegratedCircuitCaseTypeENUM item : IntegratedCircuitCaseTypeENUM.values()) {
            labels.add(item.getLabel());
        }
        return labels;
    }

    public static IntegratedCircuitCaseTypeENUM getInstance(String name) {
        for (IntegratedCircuitCaseTypeENUM item : IntegratedCircuitCaseTypeENUM.values()) {
            if (item.getLabel().equals(name)) {
                return item;
            }
        }
        return null;
    }

    public static List<?> valuesList() {
        List<IntegratedCircuitCaseTypeENUM> list = new ArrayList<>();
        list.addAll(Arrays.asList(IntegratedCircuitCaseTypeENUM.values()));
        return list;
    }
}
