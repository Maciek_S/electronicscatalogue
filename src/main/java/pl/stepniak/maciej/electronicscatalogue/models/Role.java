package pl.stepniak.maciej.electronicscatalogue.models;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public enum Role {

    ROLE_ADMIN("ADMIN"), ROLE_USER("USER");

    private String label;

    private Role(String label) {
        this.label = label;
    }

    public String getLabel() {
        return this.label;
    }

}
