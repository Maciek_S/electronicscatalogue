package pl.stepniak.maciej.electronicscatalogue.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public enum IntegratedCircuitTypeENUM {

    COMPARATOR("Komparator"),
    MICROCONTROLLER("Mikrokontroler"),
    MEMORY("Pamięć"),
    VOLTAGE_REGULATOR("Regulator napięcia"),
    LOGIC("Układ logiczny"),
    PERIPHERAL("Układ peryferyjny"),
    OPERATIONAL_AMPLIFIER("Wzmacniacz operacyjny");

    private String label;

    private IntegratedCircuitTypeENUM(String label) {
        this.label = label;
    }

    public String getLabel() {
        return this.label;
    }

    public static List<String> getLabels() {
        List<String> labels = new ArrayList<>();
        for (IntegratedCircuitTypeENUM item : IntegratedCircuitTypeENUM.values()) {
            labels.add(item.getLabel());
        }
        return labels;
    }

    public static IntegratedCircuitTypeENUM getInstance(String name) {
        for (IntegratedCircuitTypeENUM item : IntegratedCircuitTypeENUM.values()) {
            if (item.getLabel().equals(name)) {
                return item;
            }
        }
        return null;
    }

    public static List<?> valuesList() {
        List<IntegratedCircuitTypeENUM> list = new ArrayList<>();
        list.addAll(Arrays.asList(IntegratedCircuitTypeENUM.values()));
        return list;
    }
}
