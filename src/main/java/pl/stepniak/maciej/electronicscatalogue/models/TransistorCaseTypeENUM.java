package pl.stepniak.maciej.electronicscatalogue.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public enum TransistorCaseTypeENUM {

    SIP10("SIP10"),
    SO8("SO8"),
    SOP8("SOP8"),
    SOT143("SOT143"),
    SOT223("SOT223"),
    SOT23("SOT23"),
    SOT26("SOT26"),
    SOT32("SOT32"),
    SOT6("SOT6"),
    TO126("TO126"),
    TO220("TO220"),
    TO225("TO225"),
    TO247("TO247"),
    TO92("TO92");

    private String label;

    private TransistorCaseTypeENUM(String label) {
        this.label = label;
    }

    public String getLabel() {
        return this.label;
    }

    public static List<String> getLabels() {
        List<String> labels = new ArrayList<>();
        for (TransistorCaseTypeENUM item : TransistorCaseTypeENUM.values()) {
            labels.add(item.getLabel());
        }
        return labels;
    }

    public static TransistorCaseTypeENUM getInstance(String name) {
        for (TransistorCaseTypeENUM item : TransistorCaseTypeENUM.values()) {
            if (item.getLabel().equals(name)) {
                return item;
            }
        }
        return null;
    }

    public static List<?> valuesList() {
        List<TransistorCaseTypeENUM> list = new ArrayList<>();
        list.addAll(Arrays.asList(TransistorCaseTypeENUM.values()));
        return list;
    }
}
