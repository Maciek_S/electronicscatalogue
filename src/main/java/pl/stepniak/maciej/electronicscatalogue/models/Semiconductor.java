package pl.stepniak.maciej.electronicscatalogue.models;

import pl.stepniak.maciej.electronicscatalogue.models.subcategoriesEnums.SemiconductorsCategoriesENUM;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public class Semiconductor extends Component<SemiconductorsCategoriesENUM> {

    @Override
    public String getSubcategoryLabel() {
        return subcategory.getLabel();
    }

    @Override
    public String getSubcategoryName() {
        return this.subcategory.name().toLowerCase();
    }
}
