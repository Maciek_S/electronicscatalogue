package pl.stepniak.maciej.electronicscatalogue.models.subcategoriesEnums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public enum ConnectorsCategoriesENUM implements SubcategoriesEnum {

    // Elementy pasywne
    SIGNAL_CONNECTORS("Złącza sygnałowe"),
    DATA_CONNECTORS("Złącza do przesyłu danych"),
    RF_CONNECTORS("Złącza koncentryczne/RF"),
    INDUSTRY_CONNECTORS("Złącza przemysłowe"),
    POWERSUPPLY_CONNECTORS("Złącza zasilające"),
    AUDIO_AV_CONNECTORS("Złącza audio, video"),
    CABLE_TERMINALS("Konektory i końcówki kablowe"),
    OTHER_CONNECTORS("Złącza pozostałe");

    private String label;

    private ConnectorsCategoriesENUM(String label) {
        this.label = label;
    }

    public String getLabel() {
        return this.label;
    }

    public static List<String> getLabels() {
        List<String> labels = new ArrayList<>();
        for (ConnectorsCategoriesENUM item : ConnectorsCategoriesENUM.values()) {
            labels.add(item.getLabel());
        }
        return labels;
    }

    public static ConnectorsCategoriesENUM getInstance(String name) {
        for (ConnectorsCategoriesENUM item : ConnectorsCategoriesENUM.values()) {
            if (item.getLabel().equals(name)) {
                return item;
            }
        }
        return null;
    }

    public static List<?> valuesList() {
        List<ConnectorsCategoriesENUM> list = new ArrayList<>();
        list.addAll(Arrays.asList(ConnectorsCategoriesENUM.values()));
        return list;
    }

}
