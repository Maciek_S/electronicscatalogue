package pl.stepniak.maciej.electronicscatalogue.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public enum AssemblingTechnologyEnum {

    SMD, THT;

    public static List<AssemblingTechnologyEnum> getElements() {
        List<AssemblingTechnologyEnum> list = new ArrayList<>();
        list.addAll(Arrays.asList(AssemblingTechnologyEnum.values()));
        return list;
    }
}
