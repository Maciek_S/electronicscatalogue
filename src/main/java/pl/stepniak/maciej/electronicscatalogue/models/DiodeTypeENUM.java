package pl.stepniak.maciej.electronicscatalogue.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public enum DiodeTypeENUM {

    RECTIFYING("Prostownicza"),
    SCHOTTKY("Schottky"),
    UNIVERSAL("Uniwersalna"),
    TRANSIL("Zabezpieczająca"),
    ZENER("Zenera");

    private String label;

    private DiodeTypeENUM(String label) {
        this.label = label;
    }

    public String getLabel() {
        return this.label;
    }

    public static List<String> getLabels() {
        List<String> labels = new ArrayList<>();
        for (DiodeTypeENUM item : DiodeTypeENUM.values()) {
            labels.add(item.getLabel());
        }
        return labels;
    }

    public static DiodeTypeENUM getInstance(String name) {
        for (DiodeTypeENUM item : DiodeTypeENUM.values()) {
            if (item.getLabel().equals(name)) {
                return item;
            }
        }
        return null;
    }

    public static List<?> valuesList() {
        List<DiodeTypeENUM> list = new ArrayList<>();
        list.addAll(Arrays.asList(DiodeTypeENUM.values()));
        return list;
    }
}
