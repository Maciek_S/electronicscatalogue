package pl.stepniak.maciej.electronicscatalogue.models;

import java.util.HashMap;
import pl.stepniak.maciej.electronicscatalogue.models.subcategoriesEnums.PassiveComponentsCategoriesENUM;
import pl.stepniak.maciej.electronicscatalogue.models.subcategoriesEnums.SemiconductorsCategoriesENUM;
import pl.stepniak.maciej.electronicscatalogue.models.subcategoriesEnums.SubcategoriesEnum;

/**
 * Mapa wiążąc ENUM subkategorii z klasą modelu
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public class SubcategoryClassRelationship extends HashMap<SubcategoriesEnum, Class> {

    public SubcategoryClassRelationship() {
        this.put(PassiveComponentsCategoriesENUM.RESISTORS, Resistor.class);
        this.put(PassiveComponentsCategoriesENUM.CAPACITORS, Capacitor.class);
        this.put(PassiveComponentsCategoriesENUM.WARISTORS, Waristor.class);
        this.put(SemiconductorsCategoriesENUM.DIODES, Diode.class);
        this.put(SemiconductorsCategoriesENUM.INTEGRATED_CIRCUITS, IntegratedCircuit.class);
        this.put(SemiconductorsCategoriesENUM.TRANSISTORS, Transistor.class);
        // ... UZUPEŁNIĆ...
    }
}
