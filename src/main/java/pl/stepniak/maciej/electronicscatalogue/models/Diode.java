package pl.stepniak.maciej.electronicscatalogue.models;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.SafeHtml;
import pl.stepniak.maciej.electronicscatalogue.annotations.FloatMinGreaterThan;
import pl.stepniak.maciej.electronicscatalogue.models.subcategoriesEnums.SemiconductorsCategoriesENUM;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
@Entity
@Table(name = "diodes")
@NamedQueries({
    @NamedQuery(name = "Diode.findAll", query = "SELECT d FROM Diode d"),
    @NamedQuery(name = "Diode.findById", query = "SELECT d FROM Diode d WHERE d.id = :id"),
    @NamedQuery(name = "Diode.findByType", query = "SELECT d FROM Diode d WHERE d.diodeType = :type"),
    @NamedQuery(name = "Diode.countAll", query = "SELECT COUNT(d) FROM Diode d")
})
public class Diode extends Semiconductor {

    @Getter
    @Setter
    @NotNull
    @Enumerated(EnumType.STRING)
    DiodeTypeENUM diodeType;

    @Setter
    @Getter
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE, message = "{general.safeHtml}")
    @Length(min = 1, max = 50, message = "{diode.manufacturer.length}")
    String manufacturer;

    @Setter
    @Getter
    @Length(min = 1, max = 10, message = "{diode.symbol.length}")
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE, message = "{general.safeHtml}")
    String symbol;

    @Getter
    @Setter
    @NotNull
    @Enumerated(EnumType.STRING)
    AssemblingTechnologyEnum assembly;

    // Napięcie przebicia
    @Getter
    @Setter
    @FloatMinGreaterThan(min = 0, message = "{diode.breakdownVoltage.floatMinGreaterThan}")
    float breakdownVoltage;

    // Maksymalne napięcie wsteczne
    @Setter
    @Getter
    @FloatMinGreaterThan(min = 0, message = "{diode.offStateVoltageMax.floatMinGreaterThan}")
    float offStateVoltageMax;

    // Moc
    @Setter
    @Getter
    @FloatMinGreaterThan(min = 0.01F, message = "{diode.power.floatMinGreaterThan}")
    float power;

    public Diode() {
        super();
        this.label = "Dioda";
        this.unitsInStock = 0;
        this.assembly = AssemblingTechnologyEnum.SMD;
        this.subcategory = SemiconductorsCategoriesENUM.DIODES;
        this.mainCategory = MainCategoryENUM.SEMICONDUCTOR_COMPONENTS;
    }

    public String getDescription() {
        StringBuilder toReturn = new StringBuilder("");
        toReturn.append(" ").append(diodeType.getLabel());
        toReturn.append(" ").append(symbol);
        toReturn.append(" ").append(assembly.toString());

        return toReturn.toString();
    }

    @Override
    public String toString() {
        return getDescription();
    }
}
