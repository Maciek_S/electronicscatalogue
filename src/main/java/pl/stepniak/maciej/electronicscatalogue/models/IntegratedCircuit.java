package pl.stepniak.maciej.electronicscatalogue.models;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.SafeHtml;
import pl.stepniak.maciej.electronicscatalogue.models.subcategoriesEnums.SemiconductorsCategoriesENUM;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
@Entity
@Table(name = "integrated_circuits")
@NamedQueries({
    @NamedQuery(name = "IntegratedCircuit.findAll", query = "SELECT i FROM IntegratedCircuit i"),
    @NamedQuery(name = "IntegratedCircuit.findById", query = "SELECT i FROM IntegratedCircuit i WHERE i.id = :id"),
    @NamedQuery(name = "IntegratedCircuit.findBySymbol", query = "SELECT i FROM IntegratedCircuit i WHERE i.symbol = :symbol"),
    @NamedQuery(name = "IntegratedCircuit.countAll", query = "SELECT COUNT(i) FROM IntegratedCircuit i")
})
public class IntegratedCircuit extends Semiconductor {

    @Getter
    @Setter
    @NotNull
    @Enumerated(EnumType.STRING)
    IntegratedCircuitTypeENUM icType;

    @Setter
    @Getter
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE, message = "{general.safeHtml}")
    @Length(min = 1, max = 50, message = "{integratedCircuit.manufacturer.length}")
    String manufacturer;

    @Setter
    @Getter
    @Length(min = 1, max = 20, message = "{integratedCircuit.symbol.length}")
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE, message = "{general.safeHtml}")
    String symbol;

    @Setter
    @Getter
    @NotNull
    @Enumerated(EnumType.STRING)
    IntegratedCircuitCaseTypeENUM icCaseType;

    public IntegratedCircuit() {
        super();
        this.label = "Układ scalony";
        this.unitsInStock = 0;
        this.icCaseType = IntegratedCircuitCaseTypeENUM.DIP;
        this.subcategory = SemiconductorsCategoriesENUM.INTEGRATED_CIRCUITS;
        this.mainCategory = MainCategoryENUM.SEMICONDUCTOR_COMPONENTS;
    }

    public String getDescription() {
        StringBuilder toReturn = new StringBuilder("");
        toReturn.append(" ").append(icType.getLabel());
        toReturn.append(" ").append(manufacturer);
        toReturn.append(" ").append(symbol);
        toReturn.append(" ").append(icCaseType.toString());

        return toReturn.toString();
    }

    @Override
    public String toString() {
        return getDescription();
    }
}
