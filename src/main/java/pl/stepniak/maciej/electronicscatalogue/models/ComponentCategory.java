package pl.stepniak.maciej.electronicscatalogue.models;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public class ComponentCategory {//<T extends SubcategoriesEnum> {

    private MainCategoryENUM mainCategory;
    //private T subcategoryInstance;
    private String subcategory;

//    public T getSubcategoryEnum() {
//        return subcategoryEnum;
//    }
//
//    public void setSubcategoryEnum(T subcategoryEnum) {
//        this.subcategoryEnum = subcategoryEnum;
//    }
    public MainCategoryENUM getMainCategory() {
        return mainCategory;
    }

    public void setMainCategory(MainCategoryENUM mainCategory) {
        this.mainCategory = mainCategory;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }
}
