package pl.stepniak.maciej.electronicscatalogue.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public enum TransistorTypeENUM {

    IGBT("IGBT"),
    N_JFET("N-JFET"),
    N_MOSFET("N-MOSFET"),
    NPN("NPN"),
    P_JFET("P-JFET"),
    P_MOSFET("P-MOSFET"),
    PNP("PNP");

    private String label;

    private TransistorTypeENUM(String label) {
        this.label = label;
    }

    public String getLabel() {
        return this.label;
    }

    public static List<String> getLabels() {
        List<String> labels = new ArrayList<>();
        for (TransistorTypeENUM item : TransistorTypeENUM.values()) {
            labels.add(item.getLabel());
        }
        return labels;
    }

    public static TransistorTypeENUM getInstance(String name) {
        for (TransistorTypeENUM item : TransistorTypeENUM.values()) {
            if (item.getLabel().equals(name)) {
                return item;
            }
        }
        return null;
    }

    public static List<?> valuesList() {
        List<TransistorTypeENUM> list = new ArrayList<>();
        list.addAll(Arrays.asList(TransistorTypeENUM.values()));
        return list;
    }
}
