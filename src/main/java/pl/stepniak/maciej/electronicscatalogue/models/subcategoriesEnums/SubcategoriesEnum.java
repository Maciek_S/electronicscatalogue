package pl.stepniak.maciej.electronicscatalogue.models.subcategoriesEnums;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public interface SubcategoriesEnum {

    public String getLabel();
}
