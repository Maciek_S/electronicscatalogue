package pl.stepniak.maciej.electronicscatalogue.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public enum MainCategoryENUM {

    PASSIVE_COMPONENTS("Elementy pasywne"), // ok
    //    CONNECTOR_COMPONENTS("Złącza"),
    // OPTOELECTRONICS_AND_LIGHT_COMPONENTS("Optoelektronika i źródła światła"),
    SEMICONDUCTOR_COMPONENTS("Półprzewodniki");// ok
//    FUSES_AND_CURCUIT_BREAKERS("Bezpieczniki i zabezpieczenia"),
//    SWITCHES_AND_INDICATORS("Przełączniki i kontrolki"),
//    SOUND_SOURCES("Źródła dźwięku"),
//    RELAYS_AND_CONTACTRONS("Przekaźniki i styczniki"),
//    TRANSFORMERS_AND_FERRITE_CORES("Transformatory i rdzenie"),
//    FANS("Wentylatory"),
//    POWER_SOURCES("Źródła energii"),
//    ENCLOSURES("Obudowy"),
//    OTHERS("Różne");

    private String label;

    private MainCategoryENUM(String label) {
        this.label = label;
    }

    public String getLabel() {
        return this.label;
    }

    public static List<String> getLabels() {
        List<String> labels = new ArrayList<>();
        for (MainCategoryENUM item : MainCategoryENUM.values()) {
            labels.add(item.getLabel());
        }
        return labels;
    }

    public static MainCategoryENUM getInstanceFromLabel(String label) {
        for (MainCategoryENUM item : MainCategoryENUM.values()) {
            if (item.getLabel().equals(label)) {
                return item;
            }
        }
        return null;
    }

    public static MainCategoryENUM getInstanceFromNameLowerCase(String name) {
        for (MainCategoryENUM item : MainCategoryENUM.values()) {
            if (item.name().toLowerCase().equals(name)) {
                return item;
            }
        }
        return null;
    }

    public static MainCategoryENUM getInstanceFromName(String name) {
        for (MainCategoryENUM item : MainCategoryENUM.values()) {
            if (item.name().equals(name)) {
                return item;
            }
        }
        return null;
    }

    public static List<MainCategoryENUM> valuesList() {
        List<MainCategoryENUM> list = new ArrayList<>();
        list.addAll(Arrays.asList(MainCategoryENUM.values()));
        return list;
    }

}
