package pl.stepniak.maciej.electronicscatalogue.models;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public enum ResistanceUnitEnum implements UnitEnum {

    OHM("&#937;"), KOHM("k&#937;"), MOHM("M&#937;"), GOHM("G&#937;");
    private String label;

    private ResistanceUnitEnum(String label) {
        this.label = label;
    }

    public String getLabel() {
        return this.label;
    }

    public static ResistanceUnitEnum getInstance(String unit) {
        for (ResistanceUnitEnum item : ResistanceUnitEnum.values()) {
            if (item.getLabel().equals(unit)) {
                return item;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return getLabel();
    }
}
