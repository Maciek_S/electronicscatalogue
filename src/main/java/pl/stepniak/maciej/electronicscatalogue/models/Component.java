package pl.stepniak.maciej.electronicscatalogue.models;

import java.io.Serializable;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 * @param <T>
 */
@MappedSuperclass
public abstract class Component<T> implements Serializable {

    @Setter
    @Getter
    @Id
    @GeneratedValue
    protected long id;

    @Setter
    @Getter
    @Transient
    protected String label;

    @NotNull
    @Min(value = 1, message = "{component.unitInStock.min}")
    // komunikat o błędzie konwersji na int zapisany w pliku messages.properties (typeMismatch)
    @Setter
    @Getter
    protected Integer unitsInStock;

    @Transient
    protected String description;

    @Setter
    @Getter
    @Transient
    protected MainCategoryENUM mainCategory;

    @Transient
    protected T subcategory;

    public abstract String getSubcategoryLabel();

    public abstract String getSubcategoryName();

    public void setMainCategoryAndSubcategory(MainCategoryENUM mainCategory, T subcategory) {
        this.mainCategory = mainCategory;
        this.subcategory = subcategory;
    }

    public T getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(T subcategory) {
        this.subcategory = subcategory;
    }

    @Override
    public String toString() {
        return "Component{" + "id=" + id + ", label=" + label + ", unitsInStock=" + unitsInStock + ", description=" + description + ", mainCategory=" + mainCategory + ", subcategory=" + subcategory + '}';
    }

}
