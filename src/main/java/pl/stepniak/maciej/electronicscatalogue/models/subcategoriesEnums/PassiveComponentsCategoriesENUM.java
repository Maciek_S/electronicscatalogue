package pl.stepniak.maciej.electronicscatalogue.models.subcategoriesEnums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public enum PassiveComponentsCategoriesENUM implements SubcategoriesEnum {

    // Elementy pasywne
//    ANTENNAS("Anteny"),
    CAPACITORS("Kondensatory"),
    //    ENCODERS("Enkodery"),
    //    FERRITES("Ferryty EMI/EMC"),
    //    FILTERS_EMC("Filtry przeciwzakłóceniowe"),
    //    GLANDS("Dławiki"),
    //    KNOBS("Gałki"),
    //    POTENTIOMETERS("Potencjometry"),
    RESISTORS("Rezystory"),
    //    SURGE_ARRESTERS("Ochronniki przeciwprzepięciowe"),
    //    THERMISTORS("Termistory"),
    //    QUARTZ_FILTERS("Kwarce i filtry"),
    WARISTORS("Warystory");

    private String label;

    private PassiveComponentsCategoriesENUM(String label) {
        this.label = label;
    }

    @Override
    public String getLabel() {
        return this.label;
    }

    public static List<String> getLabels() {
        List<String> labels = new ArrayList<>();
        for (PassiveComponentsCategoriesENUM item : PassiveComponentsCategoriesENUM.values()) {
            labels.add(item.getLabel());
        }
        return labels;
    }

    public static PassiveComponentsCategoriesENUM getInstance(String name) {
        for (PassiveComponentsCategoriesENUM item : PassiveComponentsCategoriesENUM.values()) {
            if (item.getLabel().equals(name)) {
                return item;
            }
        }
        return null;
    }

    public static List<?> valuesList() {
        List<PassiveComponentsCategoriesENUM> list = new ArrayList<>();
        list.addAll(Arrays.asList(PassiveComponentsCategoriesENUM.values()));
        return list;
    }

}
