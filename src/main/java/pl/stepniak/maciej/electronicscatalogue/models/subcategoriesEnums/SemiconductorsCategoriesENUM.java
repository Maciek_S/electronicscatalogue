package pl.stepniak.maciej.electronicscatalogue.models.subcategoriesEnums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public enum SemiconductorsCategoriesENUM implements SubcategoriesEnum {

    // Elementy pasywne
    DIODES("Diody"),
    //    RECTIFIERS("Mostki prostownicze"),
    //    THYRISTORS("Tyrystory"),
    //    TRIACS("Triaki"),
    //    DIACS("Diaki"),
    TRANSISTORS("Tranzystory"),
    INTEGRATED_CIRCUITS("Układy scalone");

    private String label;

    private SemiconductorsCategoriesENUM(String label) {
        this.label = label;
    }

    public String getLabel() {
        return this.label;
    }

    public static List<String> getLabels() {
        List<String> labels = new ArrayList<>();
        for (SemiconductorsCategoriesENUM item : SemiconductorsCategoriesENUM.values()) {
            labels.add(item.getLabel());
        }
        return labels;
    }

    public static SemiconductorsCategoriesENUM getInstance(String name) {
        for (SemiconductorsCategoriesENUM item : SemiconductorsCategoriesENUM.values()) {
            if (item.getLabel().equals(name)) {
                return item;
            }
        }
        return null;
    }

    public static List<?> valuesList() {
        List<SemiconductorsCategoriesENUM> list = new ArrayList<>();
        list.addAll(Arrays.asList(SemiconductorsCategoriesENUM.values()));
        return list;
    }

}
