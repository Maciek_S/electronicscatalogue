package pl.stepniak.maciej.electronicscatalogue.models;

import lombok.Data;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
@Data
public class UserPassword {

    public UserPassword() {

    }

    public UserPassword(String username) {
        this.username = username;
    }

    private String username;

    private String oldPassword;

    private String newPassword;

    private String repetedPassword;
}
