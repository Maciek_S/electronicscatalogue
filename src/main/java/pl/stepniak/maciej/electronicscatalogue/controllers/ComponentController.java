package pl.stepniak.maciej.electronicscatalogue.controllers;

import java.lang.reflect.Field;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import pl.stepniak.maciej.electronicscatalogue.exceptions.NoComponentsFoundUnderCategoryException;
import pl.stepniak.maciej.electronicscatalogue.exceptions.NoSubcategoryFoundException;
import pl.stepniak.maciej.electronicscatalogue.exceptions.ToSmallComponentsInStockException;
import pl.stepniak.maciej.electronicscatalogue.models.Component;
import pl.stepniak.maciej.electronicscatalogue.services.ComponentService;
import pl.stepniak.maciej.electronicscatalogue.services.SubcategoryClassRelationshipService;

/**
 * Kontroler odpowiedzialny za obsługę zdarzeń związanych z komponentami.
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
@Controller
@RequestMapping("/components")
public class ComponentController {

    Logger logger = Logger.getLogger(this.getClass());

    @Autowired
    private ComponentService componentService;

    @Autowired
    private SubcategoryClassRelationshipService subcategoryClassRelationshipService;

    /**
     * Wyświetlanie listy wszystkich komponentów w wybranej kategorii
     *
     * @param model
     * @param mainCategory reprezentacja tekstowa kategorii głównej
     * @param subcategory reprezentacja tekstowa podkategorii
     * @param limit liczba wyświetlanych pozycji na stronę
     * @param page numer podstrony
     * @return widok listy wszystkich komponentów w wybranej
     * kategorii/podkategorii
     */
    @RequestMapping(value = "/{mainCategory}/{subcategory}", method = RequestMethod.GET)

    public String listComponentsInSubcategory(Model model, @PathVariable("mainCategory") String mainCategory,
            @PathVariable("subcategory") String subcategory, @RequestParam(name = "limit", defaultValue = "12") int limit,
            @RequestParam(name = "page", defaultValue = "1") int page) {
        List<?> components;

        int offset;
        if (page == 1) {
            offset = 0;
        } else {
            offset = limit * page - limit;
        }

        components = componentService.getComponentesInCategory(subcategory, limit, offset);
        long totalAmount = componentService.getComponentAmountInMainCategory(subcategory);

        if (components == null || components.isEmpty()) {
            throw new NoComponentsFoundUnderCategoryException();
        }

        // Przekazanie do widoku liczby elementów w podkategoriach
        componentService.putToModelSubcategoriesWithElementsAmount(model);

        int pageTotal = (int) Math.ceil(totalAmount * 1.0 / limit);

        model.addAttribute("offset", offset);
        model.addAttribute("page", page);
        model.addAttribute("pageTotal", pageTotal);
        model.addAttribute("totalAmount", totalAmount);
        model.addAttribute("components", components);

        return "components";
    }

    /**
     * Pobranie elementu - metoda zmniejsza stan magazynowy. Metoda uruchamiana
     * jest z poziomu JS (components.js).
     *
     * @param mainCategory reprezentacja tekstowa kategorii głównej
     * @param subcategory reprezentacja tekstowa podkategorii
     * @param id reprezentacja tekstowa ID wybranego komponentu
     * @param amount wartość liczbowa reprezentująca liczbę pobranych elementów
     * @return id komponentu oraz nowa wartość magazynowa w formacie:
     * id;total_amount, np. 1;2364
     */
    @RequestMapping(value = "/{mainCategory}/{subcategory}/take", method = RequestMethod.GET, produces = "text/plain")
    public @ResponseBody
    String takeComponent(@PathVariable("mainCategory") String mainCategory, @PathVariable("subcategory") String subcategory, @RequestParam String id, @RequestParam String amount) {
        int idValue = Integer.parseInt(id);
        int amountValue = Integer.parseInt(amount);

        Class clazz = subcategoryClassRelationshipService.getClassFromSubcategory(subcategory.toUpperCase()); // tu podać np. "RESISTORS"

        if (clazz == null) {
            throw new NoSubcategoryFoundException();
        }

        String newValue = componentService.takeComponent(idValue, amountValue, clazz);
        return prepareTakeAddAjaxResponse(id, newValue);
    }

    /**
     * Oddanie/dodanie elementu do magazynu - metoda zwiększa stan magazynowy.
     * Metoda uruchamiana jest z poziomu JS (components.js).
     *
     * @param mainCategory reprezentacja tekstowa kategorii głównej
     * @param subcategory reprezentacja tekstowa podkategorii
     * @param id reprezentacja tekstowa ID wybranego komponentu
     * @param amount wartość liczbowa reprezentująca liczbę zwróconych do
     * magazynu elementów
     * @return id komponentu oraz nowa wartość magazynowa w formacie:
     * id;total_amount, np. 1;2364
     */
    @RequestMapping(value = "/{mainCategory}/{subcategory}/add", method = RequestMethod.GET, produces = "text/plain")
    public @ResponseBody
    String returnComponent(@PathVariable("mainCategory") String mainCategory, @PathVariable("subcategory") String subcategory, @RequestParam String id, @RequestParam String amount) {
        int idValue = Integer.parseInt(id);
        int amountValue = Integer.parseInt(amount);

        Class clazz = subcategoryClassRelationshipService.getClassFromSubcategory(subcategory.toUpperCase()); // tu podać np. "RESISTORS"
        if (clazz == null) {
            throw new NoSubcategoryFoundException();
        }

        String newValue = componentService.returnComponent(idValue, amountValue, clazz);

        return prepareTakeAddAjaxResponse(id, newValue);
    }

    /**
     * Metoda składa ciąg znaków zawierający id komponentu oraz nowy stan
     * magazynowy.
     *
     * @param id id komponentu
     * @param newValue nowy stan magazynowy
     * @return id komponentu oraz nowa wartość magazynowa w formacie:
     * id;total_amount, np. 1;2364
     */
    private String prepareTakeAddAjaxResponse(String id, String newValue) {
        StringBuilder toReturn = new StringBuilder(id);
        toReturn.append(";");
        toReturn.append(newValue);

        return toReturn.toString();
    }

    @RequestMapping(value = "/{mainCategory}/{subcategory}/componentDetails", method = RequestMethod.GET)
    public String componentDetails(Model model, @PathVariable("mainCategory") String mainCategory,
            @PathVariable("subcategory") String subcategory, @RequestParam int id) {
        Component component;
        logger.info("SUBCATEGORY: " + subcategory);
        Class clazz = subcategoryClassRelationshipService.getClassFromSubcategory(subcategory.toUpperCase());
        if (clazz == null) {
            throw new NoSubcategoryFoundException();
        }
        component = componentService.getComponentById(id, clazz);

        // Przekazanie do widoku liczby elementów w podkategoriach
        componentService.putToModelSubcategoriesWithElementsAmount(model);

        MyArrayListToStringOverride fields = new MyArrayListToStringOverride();
        for (Field field : clazz.getDeclaredFields()) {
            fields.add(field.getName());
        }

        // dodanie pól z klasy nadrzędnej
        for (Field field : Component.class.getDeclaredFields()) {
            fields.add(field.getName());
        }

        // przekazanie atrybutów do kolejnej podstrony dodawania nowego elementu
        model.addAttribute("fields", fields);
        model.addAttribute("componentClassName", clazz.getSimpleName().toLowerCase());
        model.addAttribute("component", component);

        return "componentDetails";
    }

    @RequestMapping(value = "/{mainCategory}/{subcategory}/deleteComponent", method = RequestMethod.GET, produces = "text/plain")
    public @ResponseBody
    String deleteComponent(@PathVariable("mainCategory") String mainCategory, @PathVariable("subcategory") String subcategory, @RequestParam String id) {
        int idValue = Integer.parseInt(id);

        Class clazz = subcategoryClassRelationshipService.getClassFromSubcategory(subcategory.toUpperCase()); // tu podać np. "RESISTORS"
        if (clazz == null) {
            throw new NoSubcategoryFoundException();
        }

        componentService.deleteComponent(idValue, clazz);
        return id;
    }

    /**
     * Wyświetlanie komunikatu o braku komponentów we wskazanej kategorii
     *
     * @param exception przechwycony wyjątek
     * @return widok informujący o błędzie
     */
    @ExceptionHandler(NoComponentsFoundUnderCategoryException.class)
    public ModelAndView handleNoComponentsFoundUnderCategoryException(NoComponentsFoundUnderCategoryException exception) {
        return ExceptionController.prepareExceptionModelAndView(exception.getMessage());
    }

    /**
     * Wyświetlanie komunikatu informującego, że w magazynie nie ma
     * wystarczającej liczby komponentów.
     *
     * @param exception przechwycony wyjątek
     * @return widok informujący o błędzie
     */
    @ExceptionHandler(value = ToSmallComponentsInStockException.class)
    public ModelAndView handleToSmallComponentsInStockException(ToSmallComponentsInStockException exception) {
        return ExceptionController.prepareExceptionModelAndView(exception.getMessage());
    }
}
