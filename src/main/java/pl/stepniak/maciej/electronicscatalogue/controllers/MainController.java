package pl.stepniak.maciej.electronicscatalogue.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
@Controller
public class MainController {

    @RequestMapping("/")
    public String welcome(Model model) {
        return "welcome";
    }
}
