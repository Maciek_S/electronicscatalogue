package pl.stepniak.maciej.electronicscatalogue.controllers;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.stepniak.maciej.electronicscatalogue.exceptions.NoSubcategoryFoundException;
import pl.stepniak.maciej.electronicscatalogue.models.AssemblingTechnologyEnum;
import pl.stepniak.maciej.electronicscatalogue.models.Component;
import pl.stepniak.maciej.electronicscatalogue.models.ComponentCategory;
import pl.stepniak.maciej.electronicscatalogue.models.Diode;
import pl.stepniak.maciej.electronicscatalogue.models.DiodeTypeENUM;
import pl.stepniak.maciej.electronicscatalogue.models.IntegratedCircuit;
import pl.stepniak.maciej.electronicscatalogue.models.IntegratedCircuitCaseTypeENUM;
import pl.stepniak.maciej.electronicscatalogue.models.IntegratedCircuitTypeENUM;
import pl.stepniak.maciej.electronicscatalogue.models.MainCategoryENUM;
import pl.stepniak.maciej.electronicscatalogue.models.Transistor;
import pl.stepniak.maciej.electronicscatalogue.models.TransistorCaseTypeENUM;
import pl.stepniak.maciej.electronicscatalogue.models.TransistorTypeENUM;
import pl.stepniak.maciej.electronicscatalogue.models.subcategoriesEnums.SubcategoriesEnum;
import pl.stepniak.maciej.electronicscatalogue.services.ComponentService;
import pl.stepniak.maciej.electronicscatalogue.services.SubcategoryClassRelationshipService;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
@Controller
@RequestMapping("/addNewComponent/")
public class AddNewComponentController {

    Logger logger = Logger.getLogger(this.getClass());

    @Autowired
    private ComponentService componentService;

    @Autowired
    private SubcategoryClassRelationshipService subcategoryClassRelationshipService;

    @Autowired
    private MessageSource messageSource;

    /**
     * Przygotowanie i przekazanie odpowiedzi na zapytanie AJAX.
     *
     * @param mainCategoryLabel Etykieta tekstowa kategorii głównej
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = "/categorySelection/getCategory", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    void returnSubcategories(@RequestParam String mainCategoryLabel, HttpServletResponse response) throws IOException {

        // pobranie podkategorii na podstawie parametru żądania mainCategoryLabel
        List<SubcategoriesEnum> subcategories = componentService.getSubcategories(MainCategoryENUM.getInstanceFromLabel(mainCategoryLabel));
        JSONArray subcategoriesJSON = new JSONArray();

        subcategories.stream().forEach(item -> {
            JSONObject subcategoryJSON = new JSONObject();
            subcategoryJSON.put("label", item.getLabel());
            subcategoriesJSON.put(subcategoryJSON);
        });

        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(subcategoriesJSON.toString());
    }

    /**
     * Wyświetlenie formularza wyboru kategorii głównej oraz subkategorii.
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/categorySelection", method = RequestMethod.GET)
    public String getAddNewComponentForm(Model model) {
        ComponentCategory componentCategory = new ComponentCategory();
        model.addAttribute("componentCategory", componentCategory);
        model.addAttribute("mainCategories", MainCategoryENUM.values());
        componentService.putToModelSubcategoriesWithElementsAmount(model);
        return "addNewComponent/categorySelection";
    }

    /**
     * Zatwierdzenie wyboru kategorii głównej oraz subkategorii.
     *
     * @param componentCategory Uzupełniony model przechowujący kategorię główną
     * i subkategorię.
     * @param model
     * @param result wynik walidacji wprowadzonych danych do modelu
     * @return
     * @throws java.lang.Exception
     */
    @RequestMapping(value = "categorySelection", method = RequestMethod.POST)
    public String processAddNewComponentForm(@ModelAttribute("componentCategory") ComponentCategory componentCategory,
            Model model, BindingResult result) throws Exception {

        String subcategoryLabel = componentCategory.getSubcategory();
        MainCategoryENUM mainCategory = componentCategory.getMainCategory();
        List<SubcategoriesEnum> subcategories = componentService.getSubcategories(mainCategory);
        SubcategoriesEnum selectedSubcategory = componentService.getSubcategoryEnumFromLabel(subcategories, subcategoryLabel);

        if (selectedSubcategory != null) {
            Class clazz = subcategoryClassRelationshipService.getClassFromSubcategory(selectedSubcategory.toString());
            if (clazz == null) {
                throw new NoSubcategoryFoundException();
            }
            // pobranie listy pól klasy z modelu
            MyArrayListToStringOverride fields = new MyArrayListToStringOverride();
            for (Field field : clazz.getDeclaredFields()) {
                fields.add(field.getName());
            }

            // dodanie pól z klasy nadrzędnej
            for (Field field : Component.class.getDeclaredFields()) {
                fields.add(field.getName());
            }

            // przekazanie atrybutów do kolejnej podstrony dodawania nowego elementu
            model.addAttribute("fields", fields.toString());
            model.addAttribute("componentClassName", clazz.getSimpleName().toLowerCase());
            model.addAttribute("selectedMainCategory", mainCategory);
            model.addAttribute("selectedSubcategory", selectedSubcategory.getLabel());

            return "redirect:/addNewComponent/componentParameters";
        } else {
            throw new NoSubcategoryFoundException();
        }
    }

    /**
     * Wyświetlanie formularza, w którym użytkownik podaje parametry komponentu.
     * Lista parametrów jest określana dynamicznie w zależności od wyboru
     * podkategorii komponentu. Parametry odzwierciedlają pola klasy
     * dziedziczącej po Component.
     *
     * @param allRequestParams zawiera: 'fields', 'componentClassName',
     * 'selectedMainCategory' oraz 'selectedSubcategory'
     * @param model
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws NoSuchFieldException
     */
    @RequestMapping(value = "/componentParameters", method = RequestMethod.GET)
    public String getComponentParametersForm(@RequestParam Map<String, String> allRequestParams, Model model) throws InstantiationException, IllegalAccessException, NoSuchFieldException {

        prepareAddNewComponentForm(allRequestParams, model);

        return "addNewComponent/componentParameters";
    }

    private void prepareAddNewComponentForm(Map<String, String> allRequestParams, Model model) throws InstantiationException, IllegalAccessException {
        // Pobranie parametrow z requesta
        String fieldsFromRequest = allRequestParams.get("fields");
        String componentClassNameFromRequest = allRequestParams.get("componentClassName");
        String mainCategoryFromRequest = allRequestParams.get("selectedMainCategory");
        String subcategoryFromRequest = allRequestParams.get("selectedSubcategory");

        // Okreslenie glownej kategorii i podkategorii
        MainCategoryENUM selectedMainCategory = MainCategoryENUM.getInstanceFromName(mainCategoryFromRequest);
        List<SubcategoriesEnum> subcategoriesInMainCategory = componentService.getSubcategories(selectedMainCategory);
        SubcategoriesEnum selectedSubcategory = componentService.getSubcategoryEnumFromLabel(subcategoriesInMainCategory, subcategoryFromRequest);

        if (selectedSubcategory == null) {
            throw new NoSubcategoryFoundException();
        }

        List<String> fields = splitFieldFilenamesFromRequest(fieldsFromRequest);

        Class componentClass = subcategoryClassRelationshipService.getClassFromSubcategory(selectedSubcategory.toString());
        if (componentClass == null) {
            throw new NoSubcategoryFoundException();
        }

        Component component = createComponentInstanceWithFilledCategories(componentClass, selectedMainCategory, selectedSubcategory);

        // przekazanie do widoku parametrów
        addElementsToFormIfNecessary(componentClass, model);
        componentService.putToModelSubcategoriesWithElementsAmount(model);
        model.addAttribute("fields", fields);
        model.addAttribute("assemblingTechnologies", Arrays.asList(AssemblingTechnologyEnum.values()));
        model.addAttribute("componentClassName", componentClassNameFromRequest);
        model.addAttribute("selectedMainCategory", selectedMainCategory);
        model.addAttribute("selectedSubcategory", selectedSubcategory);
//        model.addAttribute("component", component);

    }

    private void addElementsToFormIfNecessary(Class componentClass, Model model) throws SecurityException {
        addDiodeTypesIfNecessary(componentClass, model);
        addICTypesIfNecessary(componentClass, model);
        addICCaseTypesIfNecessary(componentClass, model);
        addTransistorTypesIfNecessary(componentClass, model);
        addTransistorCaseTypesIfNecessary(componentClass, model);

        addUnitsToModel(componentClass, model);
    }

    private void addDiodeTypesIfNecessary(Class componentClass, Model model) {
        if (componentClass.isAssignableFrom(Diode.class)) {
            model.addAttribute("diodeTypes", Arrays.asList(DiodeTypeENUM.values()));
        }
    }

    private void addICTypesIfNecessary(Class componentClass, Model model) {
        if (componentClass.isAssignableFrom(IntegratedCircuit.class)) {
            model.addAttribute("icTypes", Arrays.asList(IntegratedCircuitTypeENUM.values()));
        }
    }

    private void addICCaseTypesIfNecessary(Class componentClass, Model model) {
        if (componentClass.isAssignableFrom(IntegratedCircuit.class)) {
            model.addAttribute("icCaseTypes", Arrays.asList(IntegratedCircuitCaseTypeENUM.values()));
        }
    }

    private void addTransistorTypesIfNecessary(Class componentClass, Model model) {
        if (componentClass.isAssignableFrom(Transistor.class)) {
            model.addAttribute("transistorTypes", Arrays.asList(TransistorTypeENUM.values()));
        }
    }

    private void addTransistorCaseTypesIfNecessary(Class componentClass, Model model) {
        if (componentClass.isAssignableFrom(Transistor.class)) {
            model.addAttribute("transistorCaseTypes", Arrays.asList(TransistorCaseTypeENUM.values()));
        }
    }

    private Component createComponentInstanceWithFilledCategories(Class componentClass, MainCategoryENUM selectedMainCategory, SubcategoriesEnum selectedSubcategory) throws InstantiationException, IllegalAccessException {
        Component component = (Component) componentClass.newInstance(); // utworzenie nowej instancji klasy w zaleznosci od wyboru podkategorii
        component.setMainCategory(selectedMainCategory);
        component.setSubcategory(selectedSubcategory);
        return component;
    }

    /**
     * Metoda przekazuje jednostki (pole nominalValueUnit) do modelu w
     * zależności od klasy modelu. W przypadku gdy w klasie nie ma takiego pola,
     * jest obsługiwany wyjątek - pole jest pomijane.
     *
     * @param componentClass
     * @param model
     * @throws SecurityException
     */
    private void addUnitsToModel(Class componentClass, Model model) throws SecurityException {
        // Przekazanie do modelu jednostek wartosci nominalnej w zaleznosci od klasy modelu
        try {
            Class enumClass = componentClass.getDeclaredField("nominalValueUnit").getType(); // jesli jest takie pole to sprawdz jego typ

            List<Object> enumPositions = new ArrayList<>();
            enumPositions.addAll(Arrays.asList(enumClass.getEnumConstants())); // dodaj reprezentacje tekstowa do listy

            model.addAttribute("nominalValueUnits", enumPositions); // przekaz do modelu
        } catch (NoSuchFieldException exception) {
            logger.info("W klasie " + componentClass.getSimpleName() + " brak pola o nazwie 'nominalValueUnit'.");
        }
    }

    /**
     * Metoda rozdziela nazwy pól rozdzielone przecinkami.
     *
     * @param fieldsFromRequest
     * @return Lista nazw pól
     */
    private List<String> splitFieldFilenamesFromRequest(String fieldsFromRequest) {
        List<String> fields = new ArrayList<>();
        String[] fieldsArray = fieldsFromRequest.split(";");
        fields.addAll(Arrays.asList(fieldsArray));
        return fields;
    }

    /**
     *
     * @param component
     * @param result wynik walidacji wprowadzonych danych do modelu
     * @param model
     * @param allRequestParams
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws NoSuchFieldException
     */
    @RequestMapping(value = "/componentParameters", method = RequestMethod.POST)
    public String processComponentParametersForm(@Valid @ModelAttribute("component") Component component, BindingResult result, Model model, @RequestParam Map<String, String> allRequestParams) throws InstantiationException, IllegalAccessException, NoSuchFieldException {
        if (!result.hasErrors()) {
            return processComponentParametersPOSTWhenNoErrors(result, component, model);
        } else {
            return processComponentParametersPOSTWhenErrorsOccured(result, allRequestParams, model, component);
        }

    }

    /**
     * Metoda uruchamiana w przypadku gdy walidacja wprowadzonych danych do
     * formularza dodawania nowego komponentu wykazała błędy. Metoda wyświetla
     * komunikaty błędów oraz uzupełnia formularz wcześniej wprowadzonymi
     * wartościami.
     *
     * @param result wynik walidacji wprowadzonych danych do modelu
     * @param allRequestParams
     * @param model
     * @param component
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    private String processComponentParametersPOSTWhenErrorsOccured(BindingResult result, Map<String, String> allRequestParams, Model model, Component component) throws InstantiationException, IllegalAccessException {
        prepareAddNewComponentForm(allRequestParams, model);
//        model.addAttribute("component", component);
//        model.addAttribute("errors", StringUtils.collectionToCommaDelimitedString(Utils.prepareBindingErrorsMessageCollection(result.getAllErrors())));
        return "addNewComponent/componentParameters";
    }

    /**
     * Metoda uruchamiana w przypadku gdy walidacja wprowadzonych danych do
     * formularza dodawania nowego komponentu nie wykazała błędów. Metoda po
     * sprawdzeniu czy nie było próby wiązania niedozwolonych pól wywołuje
     * metodę serwisu odpowiedzialną za dodanie nowego komponentu. W przypadku
     * powodzenia wyświetlany jest komunikat informujący, że operacja zakończyła
     * się pomyślnie.
     *
     * @param result wynik walidacji wprowadzonych danych do modelu
     * @param component
     * @param model
     * @return widok informujący o pozytywnie zakończonej operacji dodania
     * nowego komponentu
     * @throws SecurityException
     * @throws RuntimeException
     */
    private String processComponentParametersPOSTWhenNoErrors(BindingResult result, Component component, Model model) throws SecurityException, RuntimeException {
        String[] suppressedFields = result.getSuppressedFields();
        if (suppressedFields.length > 0) {
            throw new RuntimeException("Próba wiązania niedozwolonych pól:" + StringUtils.arrayToCommaDelimitedString(suppressedFields));
        }

        componentService.addComponent(component);
        componentService.putToModelSubcategoriesWithElementsAmount(model);

        return "addNewComponent/successfullyAdded";
    }

    @InitBinder("component")
    public void initialiseFillParametersFormBinder(WebDataBinder binder) {
        binder.setDisallowedFields("label", "id", "description");
    }

    /**
     * Metoda wywoływana podczas wiązania modelu oraz formularza. Tworzy
     * instancję konkretnej klasy na podstawie przekazanej w request nazwy
     * klasy.
     *
     * @param request
     * @return
     * @throws java.lang.InstantiationException
     * @throws java.lang.IllegalAccessException
     */
    @ModelAttribute("component")
    public Component getComponentInstance(final HttpServletRequest request) throws InstantiationException, IllegalAccessException {
        String componentClassName = request.getParameter("componentClassName");

        if (null != componentClassName) {
            return subcategoryClassRelationshipService.getComponentFromClassName(componentClassName);
        }
        return null;
    }
}
