package pl.stepniak.maciej.electronicscatalogue.controllers;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
class MyArrayListToStringOverride extends ArrayList {

    public MyArrayListToStringOverride() {
        super();
    }

    @Override
    public String toString() {
        String result = "";
        for (Iterator it = this.iterator(); it.hasNext();) {
            Object aThi = it.next();
            result += aThi + ";";
        }
        return result;
    }

}
