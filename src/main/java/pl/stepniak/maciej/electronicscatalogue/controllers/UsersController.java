package pl.stepniak.maciej.electronicscatalogue.controllers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import pl.stepniak.maciej.electronicscatalogue.exceptions.NewRepeatedPasswordMismatchException;
import pl.stepniak.maciej.electronicscatalogue.exceptions.OldPasswordMismatchException;
import pl.stepniak.maciej.electronicscatalogue.exceptions.ParameterNotPresentException;
import pl.stepniak.maciej.electronicscatalogue.exceptions.UnknownLoginException;
import pl.stepniak.maciej.electronicscatalogue.exceptions.UnknownRoleException;
import pl.stepniak.maciej.electronicscatalogue.models.Role;
import pl.stepniak.maciej.electronicscatalogue.models.User;
import pl.stepniak.maciej.electronicscatalogue.models.UserPassword;
import pl.stepniak.maciej.electronicscatalogue.models.UserRole;
import pl.stepniak.maciej.electronicscatalogue.services.UserService;
import pl.stepniak.maciej.electronicscatalogue.utils.Utils;

@Controller
@RequestMapping("/users")
public class UsersController {

    Logger logger = Logger.getLogger(this.getClass());

    @Autowired
    private UserService userService;

    /**
     * Metoda wyświetla listę wszystkich użytkowników.
     *
     * @param model
     * @return widok listy użytkowników
     */
    //@RolesAllowed("ROLE_ADMIN") // TODO: Sprawdzić, czemu nie działa
    @RequestMapping("/userList")
    public String userList(Model model) {
        model.addAttribute("users", userService.getAllUsers());
        return "users";
    }

    /**
     * Metoda wyświetla formularz dodawania nowego użytkownika.
     *
     * @param model
     * @return widok zawierający formularz
     */
    @RequestMapping(value = "addUser", method = RequestMethod.GET)
    public String addUserProfileShowForm(Model model) {

        User user = new User();
        model.addAttribute("user", user);
        model.addAttribute("action", "add");
        return "userProfileForm";
    }

    /**
     * Metoda obsługująca formularz dodawania nowego użytkownika (metoda POST).
     *
     * @param user obiekt użytkownika przypisany do formularza
     * @param result wynik walidacji wprowadzonych danych do modelu
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "addUser", method = RequestMethod.POST)
    public String processAddUserProfileShowForm(@Valid @ModelAttribute("user") User user,
            BindingResult result, Model model) throws Exception {
        if (!result.hasErrors()) {
            return processAddUserPOSTWhenNoErrors(result, user, model);
        } else {
            return processAddUserPOSTWhenErrorsOccured(model);
        }
    }

    /**
     * Metoda uruchamiana w przypadku gdy walidacja wprowadzonych danych do
     * formularza dodawania nowego użytkownika wykazała błędy. Metoda wyświetla
     * komunikaty błędów oraz uzupełnia formularz wcześniej wprowadzonymi
     * wartościami.
     *
     * @param model
     * @return
     */
    private String processAddUserPOSTWhenErrorsOccured(Model model) {
        model.addAttribute("action", "add");
        return "userProfileForm";
    }

    /**
     * Metoda uruchamiana w przypadku gdy walidacja wprowadzonych danych do
     * formularza dodawania nowego użytkownika nie wykazała błędów. Metoda po
     * sprawdzeniu czy nie było próby wiązania niedozwolonych pól wywołuje
     * metodę serwisu odpowiedzialną za dodanie nowego użytkowwnika. W przypadku
     * powodzenia wyświetlany jest profil nowego użytkownika.
     *
     * @param result
     * @param user obiekt użytkownika przypisany do formularza
     * @param model
     * @return
     * @throws RuntimeException
     */
    private String processAddUserPOSTWhenNoErrors(BindingResult result, User user, Model model) throws RuntimeException {
        checkSuppressedFieldsPresence(result);

        // Przypisanie roli domyślnej dla nowego użytkownika - ROLE_USER
        Set<UserRole> roles = new HashSet<>();
        UserRole startingRole = new UserRole();
        startingRole.setUser(user);
        startingRole.setRole(Role.ROLE_USER);
        roles.add(startingRole);

        user.setUserRoles(roles);
        user.setEnabled(1);

        // Zakodowanie hasła przed zapisaniem do bazy danych
        String password = user.getPassword();
        String hashedPassword = prepareEncodedPassword(password);
        user.setPassword(hashedPassword);

        // zapis użytkownika i roli
        userService.addUser(user);
        userService.addUserRole(startingRole);
        model.addAttribute("user", user);
        return "userProfile"; // po dodaniu wyświetlany profil edytowanego użytkownika
    }

    /**
     * Metoda wyświetla profil użytkownika
     *
     * @param model
     * @param userName login użytkownika, którego profil chcemy wyświetlić
     * @param request
     * @return
     */
    @RequestMapping("/userProfile")
    public String showUserProfile(Model model, @RequestParam(name = "login", defaultValue = "") String userName, HttpServletRequest request) {
        checkUserNameIsPresentInRequest(userName);
        checkUserHasPrivileges(request, userName);

        User user = userService.getUserByLogin(userName);
        model.addAttribute("user", user);
        return "userProfile";
    }

    /**
     * Metoda sprawdza czy podana nazwa użytkownika jest taka sama, jak nazwa
     * zalogowanego użytkownika. Metoda wykorzystywana do sprawdzenia, czy
     * zalogowany użytkownik modyfikuje swój własny profil.
     *
     * @param userName
     * @return true - zalogowany użytkownik edytuje swój profil
     */
    private boolean isLoggedUserChangeOwnProfile(String userName) {
        return getLoggedUserName().equals(userName);
    }

    /**
     * Metoda sprawdza czy zalogowany użytkownik posiada rolę administratora.
     *
     * @param request
     * @return true - ADMIN
     */
    private static boolean hasLoggedUserAdminRole(HttpServletRequest request) {
        return request.isUserInRole("ROLE_ADMIN");
    }

    /**
     * Metoda dodaje zalogowanego użytkownika do modelu.
     *
     * @param model
     */
    private void addLoggedUserToModel(Model model) {
        String userName = getLoggedUserName();
        addExistedUserToModel(userName, model);
    }

    /**
     * Metoda dodaje użytkownika o wybranym loginie do modelu.
     *
     * @param userName nazwa (login) użytkownika
     * @param model
     */
    private void addExistedUserToModel(String userName, Model model) {
        User user = userService.getUserByLogin(userName);
        model.addAttribute("user", user);
    }

    /**
     * Metoda pobiera nazwę zalogowanego użytkownika.
     *
     * @return nazwa (login) zalogowanego użytkownika
     */
    private String getLoggedUserName() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userName = auth.getName();
        return userName;
    }

    /**
     * Metoda wyświetla formularz edycji użytkownika, np.
     * userProfileForm?login=user1
     *
     * @param model
     * @param userName nazwa użytkownika, którego dane są wczytywane do
     * formularza
     * @param request
     * @return widok formularze edycji
     */
    @RequestMapping(value = "userProfileForm", method = RequestMethod.GET)
    public String editUserProfileShowForm(Model model, @RequestParam(name = "login", defaultValue = "") String userName, HttpServletRequest request) {
        checkUserNameIsPresentInRequest(userName);
        checkUserHasPrivileges(request, userName);

        User user = userService.getUserByLogin(userName);
        model.addAttribute("user", user);
        model.addAttribute("action", "edit");
        return "userProfileForm";
    }

    /**
     * Metoda sprawdza czy w żądaniu występuje parametr login. W metodach z
     *
     * @RequestMapping należy wykorzystać składnię z domyślnym pustym
     * parametrem, np. @RequestParam(name = "login", defaultValue = "").
     *
     * @param userName nazwa użytkownika, parametr login z żądania
     * @throws ParameterNotPresentException wyjątek do obsłużenia (błąd 400)
     */
    private void checkUserNameIsPresentInRequest(String userName) throws ParameterNotPresentException {
        if (userName.isEmpty()) {
            throw new ParameterNotPresentException();
        }
    }

    /**
     * Metoda uruchamia operacje przypisane do metody POST.
     *
     * @param user obiekt sprzężony z formularzem
     * @param result wyniki wiązania danych z formularza z encją
     * @param request
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "userProfileForm", method = RequestMethod.POST)
    public String processEditUserProfileShowForm(@Valid @ModelAttribute("user") User user,
            BindingResult result, HttpServletRequest request, Model model) throws Exception {
        return editUserLogic(user, result, model, request);
    }

    /**
     * Metoda realizuję logikę odpowiadającą za edycję profilu użytkownika.
     *
     * @param user Użytkownik, którego profil podlega edycji
     * @param result wynik walidacji wprowadzonych danych do modelu
     * @param model
     * @param request
     * @return ciąg znaków reprezentujący widok końcowy procesu edycji
     * @throws RuntimeException
     */
    private String editUserLogic(User user, BindingResult result, Model model, HttpServletRequest request) throws RuntimeException {

        checkUserHasPrivileges(request, user.getUsername());

        // Przepisz hasło, w tym formularzu nie zmieniamy hasła
        User userFromDB = userService.getUserByLogin(user.getUsername());
        user.setPassword(userFromDB.getPassword());

        // pierwsze wyrażenie w warunku po to, aby zapobiec walidacji pola password - w InitBinder ustawić pass w setDisallowedFields?
        if (((result.getErrorCount()) == 1 && (result.hasFieldErrors("password"))) || (!result.hasErrors())) {

            checkSuppressedFieldsPresence(result);

            // po dodaniu wyświetlany profil edytowanego użytkownika
            userService.updateUser(user);
            return "redirect:/users/userProfile?login=" + user.getUsername();

        } else {

            //TODO: zmienić kod korzystając z elementów Java 8
            // w przypadku gdy jest wiecej bledow niz 1, wykasuj z listy blad dotyczacy pola z haslem
            List<ObjectError> errorList = new ArrayList<>();
            if (result.hasFieldErrors("password")) {
                FieldError passwordError = result.getFieldError("password");
                for (ObjectError error : result.getAllErrors()) {
                    if (!passwordError.equals(error)) {
                        errorList.add(error);
                    }
                }
            } else {
                errorList = result.getAllErrors();
            }
            List<String> errorListMessages = Utils.prepareBindingErrorsMessageCollection(errorList);

            model.addAttribute("action", "edit");
            return "userProfileForm";
        }

//        MultipartFile productImage = user.getPhoto();
//        String extension = productImage.getOriginalFilename().split("\\.")[1];
//        String rootDirectory = request.getSession().getServletContext().getRealPath("/");
//        StringBuilder builder = new StringBuilder();
//        builder.append(rootDirectory).append(File.separator).append("img").append(File.separator);
//        builder.append("users").append(File.separator).append(user.getLogin()).append(".").append(extension);
//
//        if (!productImage.isEmpty()) {
//            try {
//                productImage.transferTo(new File(builder.toString()));
//            } catch (IOException | IllegalStateException e) {
//                throw new RuntimeException("Niepowodzenie podczas próby zapisu zdjęcia profilowego.", e);
//            }
//        }
//        return null;
    }

    /**
     * Metoda sprawdza czy w wynikach wiązania nie występuje próba wiązania
     * niedozwolonego pola określonego w initialiseBinder().
     *
     * @param result
     * @throws RuntimeException
     */
    private void checkSuppressedFieldsPresence(BindingResult result) throws RuntimeException {
        String[] suppressedFields = result.getSuppressedFields();
        if (suppressedFields.length > 0) {
            logger.error("Próba wiązania niedozwolonych pól:" + StringUtils.arrayToCommaDelimitedString(suppressedFields));
            throw new RuntimeException("Próba wiązania niedozwolonych pól:" + StringUtils.arrayToCommaDelimitedString(suppressedFields));
        }
    }

    /**
     * Metoda weryfikuje, czy zalogowany użytkownik może dokonać edycji profilu.
     * Zastosowany ręczny sposób autoryzacji, aby umożliwić użytkownikowi i
     * adminowi korzystanie z tego samego widoku pozwalającego na zmianę danych
     * profilowych (użytkownikowi swoich danych, adminowi - dowolnego innego
     * użytkownika). W przypadku braku uprawnień, rzucany jest wyjątek, a
     * operacja jest przerywana.
     *
     * @param request
     * @param user obiekt reprezentujący użytkownika, którego profil jest
     * edytowany
     * @throws AccessDeniedException
     */
    private void checkUserHasPrivileges(HttpServletRequest request, String userName) throws AccessDeniedException {
        if (!hasLoggedUserAdminRole(request)) { // // jeśli zalogowany nie jest adminem
            if (!isLoggedUserChangeOwnProfile(userName)) { // i nie edytuje swojego profilu
                throw new AccessDeniedException("Brak uprawnień."); // komunikat: niedozwolone
            }
        }
    }

    /**
     * Metoda wyświetla formularz zmiany hasła użytkownika, np.
     * changePasswordForm?login=user1
     *
     * @param model
     * @param userName nazwa użytkownika, którego dane są wczytywane do
     * formularza
     * @param request
     * @return
     */
    @RequestMapping(value = "changePasswordForm", method = RequestMethod.GET)
    public String changePasswordShowForm(Model model, @RequestParam(name = "login", defaultValue = "") String userName, HttpServletRequest request) {
        checkUserNameIsPresentInRequest(userName);
        checkUserHasPrivileges(request, userName);

        User user = userService.getUserByLogin(userName);
        UserPassword userPassword = new UserPassword(user.getUsername());

        model.addAttribute("userPassword", userPassword);
        return "changePasswordForm";
    }

    /**
     * Metoda obsługująca formularz zmiany hasła (metoda POST).
     *
     * @param userPassword model danych przechowujący pola formularza + login
     * użytkownika (username)
     * @param result
     * @param model
     * @param request
     * @return komunikaty o błędach lub widok informujący o prawidłowo
     * zmienionym haśle
     */
    @RequestMapping(value = "changePasswordForm", method = RequestMethod.POST)
    public String changePasswordShowForm(UserPassword userPassword, BindingResult result, Model model, HttpServletRequest request) {
        // odczytaj dane wejściowe
        String userName = userPassword.getUsername();
        String oldPassword = userPassword.getOldPassword();
        String newPassword = userPassword.getNewPassword();
        String repeatedPassword = userPassword.getRepetedPassword();

        // sprawdź, czy użytkownik chce zmienić swoje hasło
        checkUserHasPrivileges(request, userName);

        // odczytaj hasło z bazy danych
        User user = userService.getUserByLogin(userName);
        String passwordFromDB = user.getPassword();

        String hashedPassword = prepareEncodedPassword(newPassword);

        checkPasswordsMismatches(oldPassword, passwordFromDB, newPassword, repeatedPassword);

        // Zapisz datę zmiany hasła )moze lepiej dopisac trigger w bazie danych?)
        LocalDate locald = LocalDate.now();
        java.sql.Date timeNow = java.sql.Date.valueOf(locald);

        user.setDatepasschange(timeNow);

        // zapisz nowe hasło
        user.setPassword(hashedPassword);
        userService.updateUser(user);

        model.addAttribute("username", userName);
        return "passwordSuccessfullyChanged";
    }

    /**
     * Metoda konwertuje hasło w postaci plain do postaci zakodowanej z
     * zastosowanie BCrypt.
     *
     * @param password hasło w postaci plain
     * @return zakodowane hasło
     */
    private String prepareEncodedPassword(String password) {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(password);
        return hashedPassword;
    }

    /**
     * Metoda sprawdza spójność haseł. Jeśli stare hasło nie zgadza się z tym
     * przechowywanym w bazie danych lub gdy powtórnie wpisane nowe hasło się
     * nie zgadza z nowym hasłem, rzucane są wyjątki.
     *
     * @param oldPassword wpisane w formularzu stare hasło w celu weryfikacji
     * @param passwordFromDB hasło odczytane z bazy danych
     * @param newPassword nowe hasło wpisane w formularzu
     * @param repeatedPassword powtórnie wpisane nowe hasło
     * @throws NewRepeatedPasswordMismatchException
     * @throws OldPasswordMismatchException
     */
    private void checkPasswordsMismatches(String oldPassword, String passwordFromDB, String newPassword, String repeatedPassword) throws NewRepeatedPasswordMismatchException, OldPasswordMismatchException {
        // sprawdź czy stare hasło jest OK
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        if (!passwordEncoder.matches(oldPassword, passwordFromDB)) {
            throw new OldPasswordMismatchException();
        }

        // sprawdź czy powtórzone hasło jest ok
        if (!newPassword.equals(repeatedPassword)) {
            throw new NewRepeatedPasswordMismatchException();
        }
    }

    @RequestMapping(value = "/addRole", method = RequestMethod.GET, produces = "text/plain")
    public @ResponseBody
    String addRoleAjax(@RequestParam(name = "login", defaultValue = "") String userName, @RequestParam(name = "roleName", defaultValue = "") String roleName, HttpServletRequest request, Model model) throws Exception {
        return executeAddRole(userName, request, roleName, model);
    }

    @RequestMapping(value = "/addRole")
    public String addRole(@RequestParam(name = "login", defaultValue = "") String userName, @RequestParam(name = "roleName", defaultValue = "") String roleName, HttpServletRequest request, Model model) throws Exception {
        return executeAddRole(userName, request, roleName, model);
    }

    /**
     * Metoda realizuje operację przyznawania użytkownikowi nowej roli.
     *
     * @param userName
     * @param request
     * @param roleName
     * @param model
     * @return
     * @throws ParameterNotPresentException
     * @throws UnknownRoleException
     */
    private String executeAddRole(String userName, HttpServletRequest request, String roleName, Model model)
            throws ParameterNotPresentException, UnknownRoleException {
        checkUserNameIsPresentInRequest(userName);
        checkRoleIsPresentInRequest(roleName, request);

        // odczytaj stan dotychczasowy
        User user = userService.getUserByLogin(userName);
        Set<UserRole> roles = user.getUserRoles();

        // dodaj nową rolę
        UserRole newRole = new UserRole();
        try {
            Role role = Role.valueOf(roleName);
            newRole.setUser(user);
            newRole.setRole(role);
            if (!userHasRole(roles, role)) {
                roles.add(newRole);
                user.setUserRoles(roles);
                userService.updateUser(user);
                userService.addUserRole(newRole);
            }

        } catch (IllegalArgumentException ex) {
            throw new UnknownRoleException();
        }
        return "redirect:/users/userProfileForm?login=" + userName;
    }

    /**
     * Metoda służy do sprawdzenia czy dany użytkownik posiada już wybraną rolę.
     *
     * @param roles zbiór obiektów wiążących użytkownika z rolami
     * @param role rola, której obecność w zbiorze roles chceny sprawdzić
     * @return
     */
    private boolean userHasRole(Set<UserRole> roles, Role role) {
        return roles.stream().anyMatch((userRole) -> (userRole.getRole().equals(role)));
    }

    /**
     * Metoda realizuje operację pozbawienia użytkownika roli.
     *
     * @param userName
     * @param roleName
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/removeRole")
    public String removeRole(@RequestParam(name = "login", defaultValue = "") String userName,
            @RequestParam(name = "roleName", defaultValue = "") String roleName, HttpServletRequest request) throws Exception {
        checkUserNameIsPresentInRequest(userName);
        checkRoleIsPresentInRequest(roleName, request);

        // odczytaj stan dotychczasowy
        User user = userService.getUserByLogin(userName);
        Set<UserRole> roles = user.getUserRoles();
        UserRole roleToRemove = null;
        try {
            Role role = Role.valueOf(roleName);

            for (UserRole currentRole : roles) {
                if (currentRole.getRole() == role) {
                    roleToRemove = currentRole;
                }
            }
            if (roleToRemove != null) {
                roles.remove(roleToRemove);
                user.setUserRoles(roles);
                userService.updateUser(user);
                userService.deleteUserRole(roleToRemove);
            }

        } catch (IllegalArgumentException ex) {
            throw new UnknownRoleException();
        }
        return "redirect:/users/userProfileForm?login=" + userName;
    }

    /**
     * Metoda sprawdza czy w żądaniu znajduje się rola do wykasowania.
     *
     * @param roleName nazwa roli
     * @param request
     */
    private void checkRoleIsPresentInRequest(String roleName, HttpServletRequest request) {
        if (roleName.isEmpty()) {
            throw new ParameterNotPresentException();
        }
    }

    /**
     * Metoda wywoływana za pomocą AJAXa, służy do zmiany stanu flagi
     * zablokowania użytkownika.
     *
     * @param request
     * @param username login użytkownika, którego dotyczy żądanie
     * @param lock stan blokady - true oznacza blokadę, false oznacza brak
     * blokady
     * @return ciąg znaków username;lock, np. "maciejs;false" oznacza brak
     * blokady konta użytkownika maciejs
     */
    @RequestMapping(value = "/lockUser", method = RequestMethod.GET, produces = "text/plain")
    public @ResponseBody
    String lockUser(HttpServletRequest request, @RequestParam(name = "login", defaultValue = "") String username,
            @RequestParam(name = "lock", defaultValue = "true") boolean lock) {
        checkUserNameIsPresentInRequest(username);
        checkUserHasPrivileges(request, username);

        User user = userService.getUserByLogin(username);
        userService.setLockUserState(user, lock);
        StringBuilder builder = new StringBuilder(username);
        builder.append(";").append(lock);
        return builder.toString();
    }

    /**
     * Metoda obsługująca wyjątek rzucany w przypadku, gdy użytkownik wprowadził
     * błędne stare hasło w przypadku próby zmiany hasła.
     *
     * @param exception przechwycony wyjątek
     * @return widok informujący o błędzie
     */
    @ExceptionHandler(value = OldPasswordMismatchException.class)
    public ModelAndView handleOldPasswordMismatchException(OldPasswordMismatchException exception) {
        return ExceptionController.prepareExceptionModelAndView(exception.getMessage());
    }

    /**
     * Metoda obsługująca wyjątek rzucany w przypadku, gdy użytkownik wprowadził
     * nieprawidłowo powtórne hasło.
     *
     * @param exception przechwycony wyjątek
     * @return widok informujący o błędzie
     */
    @ExceptionHandler(value = NewRepeatedPasswordMismatchException.class)
    public ModelAndView handleNewRepeatedPasswordMismatchException(NewRepeatedPasswordMismatchException exception) {
        return ExceptionController.prepareExceptionModelAndView(exception.getMessage());
    }

    /**
     * Metoda obsługująca wyjątek rzucany w przypadku, gdy w żądaniu występuje
     * nieznana rola.
     *
     * @param exception przechwycony wyjątek
     * @return widok informujący o błędzie
     */
    @ExceptionHandler(value = UnknownRoleException.class)
    public ModelAndView handleUnknownRoleException(UnknownRoleException exception) {
        return ExceptionController.prepareExceptionModelAndView(exception.getMessage());
    }

    /**
     * Metoda obsługująca wyjątek rzucany w przypadku, gdy w żądaniu występuje
     * nieznana nazwa użytkownika (login).
     *
     * @param exception przechwycony wyjątek
     * @return widok informujący o błędzie
     */
    @ExceptionHandler(value = UnknownLoginException.class)
    public ModelAndView handleUnknownLoginException(UnknownLoginException exception) {
        return ExceptionController.prepareExceptionModelAndView(exception.getMessage());
    }
}
