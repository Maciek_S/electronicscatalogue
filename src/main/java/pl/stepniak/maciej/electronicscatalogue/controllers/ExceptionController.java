package pl.stepniak.maciej.electronicscatalogue.controllers;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;
import pl.stepniak.maciej.electronicscatalogue.exceptions.NoSubcategoryFoundException;
import pl.stepniak.maciej.electronicscatalogue.exceptions.PageNotFoundException;
import pl.stepniak.maciej.electronicscatalogue.exceptions.ParameterNotPresentException;
import pl.stepniak.maciej.electronicscatalogue.exceptions.UnknownComponentIdException;

/**
 * Kontroler odpowiedzialny za obsługę wyjątków.
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
@Controller
@ControllerAdvice
public class ExceptionController {

    Logger logger = Logger.getLogger(this.getClass());

    /**
     * Metoda obsługi wyjątku. Błąd 404 - brak zasobu.
     *
     * @param exception przechwycony wyjątek
     * @return widok informujący o błędzie
     */
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = NoHandlerFoundException.class)
    public ModelAndView handlePageNotFoundException(NoHandlerFoundException exception) {
        String exceptionMessage = "";

        if (exception.getClass().getSuperclass().isAssignableFrom(PageNotFoundException.class)) {
            exceptionMessage = PageNotFoundException.message;
        }

        return prepareExceptionModelAndView(exceptionMessage);
    }

    /**
     * Metoda obsługi wyjątku. Błąd 403 - brak uprawnień do przeglądania zasobu.
     *
     * @return widok informujący o błędzie
     */
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(value = AccessDeniedException.class)
    public ModelAndView handleAccessDeniedException() {
        String exceptionMessage = "Brak uprawnień do przeglądania tego zasobu.";
        return prepareExceptionModelAndView(exceptionMessage);
    }

    /**
     * Metoda obsługi wyjątku. Błąd 400 - parametr nie jest obecny w żądaniu.
     *
     * @return widok informujący o błędzie
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = ParameterNotPresentException.class)
    public ModelAndView handleBadRequestException() {
        String exceptionMessage = "Nieprawidłowe zapytanie – żądanie nie może "
                + "być obsłużone przez serwer z powodu nieprawidłowości "
                + "postrzeganej jako błąd użytkownika (np. błędna składnia zapytania, brak wymaganego parametru).";
        return prepareExceptionModelAndView(exceptionMessage);
    }

    /**
     * Metoda obsługi wyjątku. Błąd 403 - brak uprawnień do oglądania. Obsługa
     * &lt;security:access-denied-handler&gt; w security-context.xml.
     *
     * @return widok informujący o błędzie
     */
    @RequestMapping("/accessDenied")
    public ModelAndView handleAccessIsDenied() {
        String exceptionMessage = "Security: Brak uprawnień do przeglądania tego zasobu.";
        return prepareExceptionModelAndView(exceptionMessage);
    }

    /**
     * Metoda obsługi wyjątku. Nie znaleziono podkategorii.
     *
     * @param exception przechwycony wyjątek
     * @return widok informujący o błędzie
     */
    @ExceptionHandler(value = NoSubcategoryFoundException.class)
    public ModelAndView handleNoSubcategoryFoundException(NoSubcategoryFoundException exception) {
        return prepareExceptionModelAndView(exception.getMessage());
    }

    /**
     * Metoda obsługi wyjątku. Nie znaleziono komponentu o podanym ID lub
     * zapytanie nie wskazuje jednoznacznie na jeden komponent.
     *
     * @param exception przechwycony wyjątek
     * @return widok informujący o błędzie
     */
    @ExceptionHandler(value = UnknownComponentIdException.class)
    public ModelAndView handleUnknownComponentIdException(UnknownComponentIdException exception) {
        String exceptionMessage = "Komponent o podanym ID nie istnieje w bazie danych.";
        return prepareExceptionModelAndView(exceptionMessage);
    }

    /**
     * Metoda obsługi wyjątku. Błąd przy konwertowaniu ciągu tekstowego na
     * liczbę.
     *
     * @param exception przechwycony wyjątek
     * @return widok informujący o błędzie
     */
    @ExceptionHandler(value = NumberFormatException.class)
    public ModelAndView handleNumberFormatException(NumberFormatException exception) {
        String exceptionMessage = "Wprowadzono dane w nieprawidłowym formacie liczbowym.";
        return prepareExceptionModelAndView(exceptionMessage);
    }

    /**
     * Metoda wyświetla widok komunikatu błędu wraz z przekazanym do niego
     * komunikatem błędu.
     *
     * @param exceptionMessage komunikat błędu
     * @return widok informujący o błędzie
     */
    public static ModelAndView prepareExceptionModelAndView(String exceptionMessage) {
        ModelAndView mav = new ModelAndView();
        mav.addObject("exceptionMessage", exceptionMessage);
        mav.setViewName("error");
        return mav;
    }
}
