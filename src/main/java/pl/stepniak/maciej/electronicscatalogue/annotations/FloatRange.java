package pl.stepniak.maciej.electronicscatalogue.annotations;

import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.FIELD;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import pl.stepniak.maciej.electronicscatalogue.validators.FloatRangeValidator;

/**
 * Adnotacja wykorzystywana do oznaczania pola modelu przy walidacji. Test:
 * float posiada wartość zawartą w określonum przedziale minimum-maksimum.
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = FloatRangeValidator.class)
@Documented
public @interface FloatRange {

    String message() default "Pole zawiera wartość z niedozwolonego przedziału";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    float min() default 0;

    float max() default Float.MAX_VALUE;
}
