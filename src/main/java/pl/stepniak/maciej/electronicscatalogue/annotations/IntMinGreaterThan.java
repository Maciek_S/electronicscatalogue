package pl.stepniak.maciej.electronicscatalogue.annotations;

import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.FIELD;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import pl.stepniak.maciej.electronicscatalogue.validators.IntMinGreaterThanValidator;

/**
 * Adnotacja wykorzystywana do oznaczania pola modelu przy walidacji. Test: czy
 * int zawiera wartość większą niż określone minimum. Alternatywa dla @Min,
 * które jest 'greater than or equal'
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = IntMinGreaterThanValidator.class)
@Documented
public @interface IntMinGreaterThan {

    String message() default "Pole zawiera za małą wartość";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    int min() default 0;
}
