package pl.stepniak.maciej.electronicscatalogue.annotations;

import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.FIELD;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import pl.stepniak.maciej.electronicscatalogue.validators.FloatMinGreaterThanValidator;

/**
 * Adnotacja wykorzystywana do oznaczania pola modelu przy walidacji. Test: czy
 * float zawiera wartość większą niż określone minimum.
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = FloatMinGreaterThanValidator.class)
@Documented
public @interface FloatMinGreaterThan {

    String message() default "Pole zawiera za małą wartość";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    float min() default 0;
}
