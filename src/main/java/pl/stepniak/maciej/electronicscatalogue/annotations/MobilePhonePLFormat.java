package pl.stepniak.maciej.electronicscatalogue.annotations;

import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.FIELD;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import pl.stepniak.maciej.electronicscatalogue.validators.MobilePhonePLFormatValidator;

/**
 * Adnotacja wykorzystywana do oznaczania pola modelu przy walidacji. Test:
 * sprawdzenie poprawności formatu telefonu komórkowego. Prawidłowe formaty:
 * xxx-xxx-xxx lub xxxxxxxxx.
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = MobilePhonePLFormatValidator.class)
@Documented
public @interface MobilePhonePLFormat {

    String message() default "Prawidłowy format telefonu komórkowego: xxx-xxx-xxx lub xxxxxxxxx";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
