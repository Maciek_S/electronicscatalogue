package pl.stepniak.maciej.electronicscatalogue.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import pl.stepniak.maciej.electronicscatalogue.annotations.DimensionFormatXYZ;

/**
 * Klasa walidatora, potrzebna do sprawdzenia czy pole zawiera numer telefonu
 * komórkowego w odpowiednim formacie. Prawidłowe formaty: xxx-xxx-xxx lub
 * xxxxxxxxx.
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public class DimensionFormatXYZValidator implements ConstraintValidator<DimensionFormatXYZ, String> {

    @Override
    public void initialize(DimensionFormatXYZ dimension) {

    }

    @Override
    public boolean isValid(String t, ConstraintValidatorContext cvc) {
        boolean isValid;

        if (t.isEmpty()) {
            return false;
        }
        // (dł. x szer. x wys.)
        String number = "[0-9]*\\.?[0-9]*";
        String separator = "\\s?x\\s?";

        String dimensionXYZPattern = "^(" + number + ")(" + separator + ")(" + number + ")(" + separator + ")(" + number + ")$";

        Pattern pattern = Pattern.compile(dimensionXYZPattern);
        Matcher matcher = pattern.matcher(t);

        isValid = matcher.matches();

        return isValid;
    }

}
