package pl.stepniak.maciej.electronicscatalogue.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import pl.stepniak.maciej.electronicscatalogue.annotations.FloatMinGreaterThan;

/**
 * Klasa walidatora, potrzebna do sprawdzenia czy pole klasy typu float zawiera
 * wartość większą niż min.
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public class FloatMinGreaterThanValidator implements ConstraintValidator<FloatMinGreaterThan, Float> {

    private float min;

    @Override
    public void initialize(FloatMinGreaterThan a) {
        this.min = a.min();
    }

    @Override
    public boolean isValid(Float f, ConstraintValidatorContext cvc) {

        boolean isValid;
        if (f != null) {
            isValid = (f > min);
        } else {
            isValid = false;
        }

        return isValid;

    }
}
