package pl.stepniak.maciej.electronicscatalogue.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import pl.stepniak.maciej.electronicscatalogue.annotations.MobilePhonePLFormat;

/**
 * Klasa walidatora, potrzebna do sprawdzenia czy pole zawiera numer telefonu
 * komórkowego w odpowiednim formacie. Prawidłowe formaty: xxx-xxx-xxx lub
 * xxxxxxxxx.
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public class MobilePhonePLFormatValidator implements ConstraintValidator<MobilePhonePLFormat, String> {

    @Override
    public void initialize(MobilePhonePLFormat a) {

    }

    @Override
    public boolean isValid(String t, ConstraintValidatorContext cvc) {
        if (t.isEmpty()) {
            return true;
        }

        boolean isValid;

        String mobilePhonePattern = "^(\\d{3})(-?)(\\d{3})(-?)(\\d{3})$";

        Pattern pattern = Pattern.compile(mobilePhonePattern);
        Matcher matcher = pattern.matcher(t);

        isValid = matcher.matches();

        return isValid;
    }

}
