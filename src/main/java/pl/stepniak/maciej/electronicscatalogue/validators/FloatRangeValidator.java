package pl.stepniak.maciej.electronicscatalogue.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import pl.stepniak.maciej.electronicscatalogue.annotations.FloatRange;

/**
 * Klasa walidatora, potrzebna do sprawdzenia czy pole klasy typu float zawiera
 * wartość z przedziału min-max.
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public class FloatRangeValidator implements ConstraintValidator<FloatRange, Float> {

    private float max;
    private float min;

    @Override
    public void initialize(FloatRange a) {
        this.min = a.min();
        this.max = a.max();
    }

    @Override
    public boolean isValid(Float f, ConstraintValidatorContext cvc) {

        boolean isValid;
        if (f != null) {
            isValid = !((f < min) || (f > max));
        } else {
            isValid = false;
        }

        return isValid;

    }

}
