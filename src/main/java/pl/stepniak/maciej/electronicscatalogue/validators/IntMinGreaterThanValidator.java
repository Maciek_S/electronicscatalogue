package pl.stepniak.maciej.electronicscatalogue.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import pl.stepniak.maciej.electronicscatalogue.annotations.IntMinGreaterThan;

/**
 * Klasa walidatora, potrzebna do sprawdzenia czy pole klasy typu int zawiera
 * wartość większą niż min. Alternatywa dla @Min, które jest 'greater than or
 * equal'
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public class IntMinGreaterThanValidator implements ConstraintValidator<IntMinGreaterThan, Integer> {

    private int min;

    @Override
    public void initialize(IntMinGreaterThan a) {
        this.min = a.min();
    }

    @Override
    public boolean isValid(Integer i, ConstraintValidatorContext cvc) {

        boolean isValid;
        if (i != null) {
            isValid = (i > min);
        } else {
            isValid = false;
        }

        return isValid;

    }
}
