package pl.stepniak.maciej.electronicscatalogue.utils;

import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.ObjectError;

/**
 * Klasa narzędziowa z metodami statycznymi.
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public class Utils {

    /**
     * Metoda konwertuje listę obiektów typu ObjectError na listę obiektów typu
     * String przechowującą tekstowe komunikaty błędów.
     *
     * @param errors List&lt;ObjectError&gt; lista błędów z obiektu typu
     * BindingResult
     * @return List&lt;String&gt; wejściowa lista błędów przekonwertowana na typ
     * String
     */
    public static List<String> prepareBindingErrorsMessageCollection(List<ObjectError> errors) {
        List<String> messages = new ArrayList<>();
        errors.forEach(error -> messages.add(error.getDefaultMessage()));
        return messages;
    }
}
