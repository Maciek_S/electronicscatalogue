package pl.stepniak.maciej.electronicscatalogue.services;

import pl.stepniak.maciej.electronicscatalogue.models.User;
import pl.stepniak.maciej.electronicscatalogue.models.UserRole;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public interface UserService {

    public User getUserByLogin(String login);

    public Iterable<User> getAllUsers();

    public void addUser(User user);

    public void updateUser(User user);

    public void addUserRole(UserRole role);

    public void deleteUserRole(UserRole role);

    public void setLockUserState(User user, boolean lock);
}
