/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.stepniak.maciej.electronicscatalogue.services;

import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import pl.stepniak.maciej.electronicscatalogue.models.Component;
import pl.stepniak.maciej.electronicscatalogue.models.SubcategoryClassRelationship;
import pl.stepniak.maciej.electronicscatalogue.models.subcategoriesEnums.SubcategoriesEnum;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
@Service
public class SubcategoryClassRelationshipService {

    Logger logger = Logger.getLogger(this.getClass());

    private final SubcategoryClassRelationship map;

    public SubcategoryClassRelationshipService() {
        this.map = new SubcategoryClassRelationship();
    }

    public Class getClassFromSubcategory(String subcategoryLabel) {
        for (Map.Entry<SubcategoriesEnum, Class> entry : map.entrySet()) {
            Class clazz = entry.getValue();
            SubcategoriesEnum emun = entry.getKey();
            if (subcategoryLabel.equals(emun.toString())) {

                return clazz;
            }
        }
        return null;
    }

    public Class getClassFromClassName(String className) {
        for (Map.Entry<SubcategoriesEnum, Class> entry : map.entrySet()) {
            Class clazz = entry.getValue();
            SubcategoriesEnum emun = entry.getKey();
            if (className.equals(clazz.getSimpleName().toLowerCase())) {
                return clazz;
            }
        }
        return null;
    }

    public Component getComponentFromClassName(String className) throws InstantiationException, IllegalAccessException {
        for (Map.Entry<SubcategoriesEnum, Class> entry : map.entrySet()) {
            Class clazz = entry.getValue();
            if (className.equals(clazz.getSimpleName().toLowerCase())) {
                return (Component) clazz.newInstance();
            }
        }
        return null;
    }
}
