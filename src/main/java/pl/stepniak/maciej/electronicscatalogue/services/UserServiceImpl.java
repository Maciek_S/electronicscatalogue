package pl.stepniak.maciej.electronicscatalogue.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.stepniak.maciej.electronicscatalogue.exceptions.UnknownLoginException;
import pl.stepniak.maciej.electronicscatalogue.models.User;
import pl.stepniak.maciej.electronicscatalogue.models.UserRole;
import pl.stepniak.maciej.electronicscatalogue.repositories.UserRepository;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    Logger logger = Logger.getLogger(this.getClass());

    @Autowired
    private UserRepository userRepository;

    @Override
    public User getUserByLogin(String login) {
        User user = userRepository.findOne(login);
        if (user == null) {
            throw new UnknownLoginException();
        }
        return user;
    }

    @Override
    public Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public void addUser(User user) {
        userRepository.save(user);
    }

    @Override
    public void updateUser(User user) {
        userRepository.updateUser(user);
    }

    @Override
    public void addUserRole(UserRole role) {
        userRepository.saveRole(role);
    }

    @Override
    public void deleteUserRole(UserRole role) {
        userRepository.deleteRole(role);
    }

    @Override
    public void setLockUserState(User user, boolean lock) {
        if (lock) {
            user.setEnabled(0);
        } else {
            user.setEnabled(1);
        }

        userRepository.updateUser(user);
    }
}
