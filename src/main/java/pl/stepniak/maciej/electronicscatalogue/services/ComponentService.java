package pl.stepniak.maciej.electronicscatalogue.services;

import java.util.List;
import java.util.Map;
import org.springframework.ui.Model;
import pl.stepniak.maciej.electronicscatalogue.models.Component;
import pl.stepniak.maciej.electronicscatalogue.models.MainCategoryENUM;
import pl.stepniak.maciej.electronicscatalogue.models.subcategoriesEnums.SubcategoriesEnum;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
public interface ComponentService {

    public Component getComponentById(int id, Class clazz);

    public String takeComponent(int id, int count, Class clazz);

    public String returnComponent(int id, int count, Class clazz);

    public void deleteComponent(int id, Class clazz);

    public List<? extends Component> getComponentesInCategory(String category, int limit, int offset);

    public long getComponentAmountInMainCategory(String subcategory);

    public List<String> getSubcategoriesLabels(MainCategoryENUM mainCategory);

    public List<SubcategoriesEnum> getSubcategories(MainCategoryENUM mainCategory);

    public Map<Object, Long> getSubcategoriesWithNumberOfElements(MainCategoryENUM mainCategory);

    void addComponent(Component component);

    public SubcategoriesEnum getSubcategoryEnumFromLabel(List<SubcategoriesEnum> subcategories, String subcategoryLabel);

    public void putToModelSubcategoriesWithElementsAmount(Model model);
}
