package pl.stepniak.maciej.electronicscatalogue.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import pl.stepniak.maciej.electronicscatalogue.exceptions.NoSubcategoryFoundException;
import pl.stepniak.maciej.electronicscatalogue.exceptions.ToSmallComponentsInStockException;
import pl.stepniak.maciej.electronicscatalogue.exceptions.UnknownComponentIdException;
import pl.stepniak.maciej.electronicscatalogue.models.Component;
import pl.stepniak.maciej.electronicscatalogue.models.MainCategoryENUM;
import pl.stepniak.maciej.electronicscatalogue.models.subcategoriesEnums.PassiveComponentsCategoriesENUM;
import pl.stepniak.maciej.electronicscatalogue.models.subcategoriesEnums.SemiconductorsCategoriesENUM;
import pl.stepniak.maciej.electronicscatalogue.models.subcategoriesEnums.SubcategoriesEnum;
import pl.stepniak.maciej.electronicscatalogue.repositories.ComponentRepository;

/**
 *
 * @author Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
 */
@Service
@Transactional
public class ComponentServicesImpl implements ComponentService {

    Logger logger = Logger.getLogger(this.getClass());

    @Autowired
    private ComponentRepository componentRepository;

    @Autowired
    private SubcategoryClassRelationshipService subcategoryClassRelationshipService;

    @Override
    public String takeComponent(int id, int count, Class clazz) {

        Component componentById = componentRepository.getComponentById(id, clazz);

        if (componentById.getUnitsInStock() < count) {
            throw new ToSmallComponentsInStockException();
        }
        int newValue = componentById.getUnitsInStock() - count;
        componentById.setUnitsInStock(newValue);

        componentRepository.save(componentById);

        return String.valueOf(newValue);
    }

    @Override
    public String returnComponent(int id, int count, Class clazz) {
        Component componentById = componentRepository.getComponentById(id, clazz);
        int newValue = componentById.getUnitsInStock() + count;
        componentById.setUnitsInStock(newValue);

        componentRepository.save(componentById);

        return String.valueOf(newValue);
    }

    @Override
    public Component getComponentById(int id, Class clazz) {
        return componentRepository.getComponentById(id, clazz);
    }

    @Override
    public List<? extends Component> getComponentesInCategory(String subcategory, int limit, int offset) {
        Class clazz = subcategoryClassRelationshipService.getClassFromSubcategory(subcategory.toUpperCase()); // tu podać np. "RESISTORS"

        if (clazz == null) {
            throw new NoSubcategoryFoundException();
        }

        String className = clazz.getSimpleName();

        return componentRepository.getComponentsInCategory(className, limit, offset);
    }

    @Override
    public long getComponentAmountInMainCategory(String subcategory) {
        Class clazz = subcategoryClassRelationshipService.getClassFromSubcategory(subcategory.toUpperCase()); // tu podać np. "RESISTORS"

        if (clazz == null) {
            throw new NoSubcategoryFoundException();
        }

        String className = clazz.getSimpleName();
        return componentRepository.getComponentsAmountInCategory(className);
    }

    @Override
    public Map<Object, Long> getSubcategoriesWithNumberOfElements(MainCategoryENUM mainCategory) {
        Map<Object, Long> subcategories = new TreeMap<>();
        switch (mainCategory) {
            case PASSIVE_COMPONENTS: {
                for (PassiveComponentsCategoriesENUM item : PassiveComponentsCategoriesENUM.values()) {
                    subcategories.put(item, getComponentAmountInMainCategory(item.name().toLowerCase()));
                }
                break;
            }
            case SEMICONDUCTOR_COMPONENTS: {
                for (SemiconductorsCategoriesENUM item : SemiconductorsCategoriesENUM.values()) {
                    subcategories.put(item, getComponentAmountInMainCategory(item.name().toLowerCase()));
                }
                break;
            }
            default: {
                for (SemiconductorsCategoriesENUM item : SemiconductorsCategoriesENUM.values()) {
                    subcategories.put(item, getComponentAmountInMainCategory(item.name().toLowerCase()));
                }
            }
        }

        return subcategories;
    }

    // TODO: TRZEBA ZMIENIC, JAKIS WZORZEC POWINIEN BYC, FABRYKA?
    @Override
    public List<String> getSubcategoriesLabels(MainCategoryENUM mainCategory) {
        ArrayList<String> subcategories = new ArrayList<>();
        switch (mainCategory) {
            case PASSIVE_COMPONENTS: {
                for (PassiveComponentsCategoriesENUM item : PassiveComponentsCategoriesENUM.values()) {
                    subcategories.add(item.getLabel());
                }
                break;
            }
            case SEMICONDUCTOR_COMPONENTS: {
                for (SemiconductorsCategoriesENUM item : SemiconductorsCategoriesENUM.values()) {
                    subcategories.add(item.getLabel());
                }
                break;
            }
            default: {
                for (SemiconductorsCategoriesENUM item : SemiconductorsCategoriesENUM.values()) {
                    subcategories.add(item.getLabel());
                }
            }
        }

        return subcategories;
    }

    @Override
    public void addComponent(Component component) {
        componentRepository.save(component);
    }

    // TODO: TRZEBA ZMIENIC, JAKIS WZORZEC POWINIEN BYC, FABRYKA?
    @Override
    public List<SubcategoriesEnum> getSubcategories(MainCategoryENUM mainCategory) {

        switch (mainCategory) {
            case PASSIVE_COMPONENTS: {
                ArrayList<SubcategoriesEnum> subcategories = new ArrayList<>();
                subcategories.addAll(Arrays.asList(PassiveComponentsCategoriesENUM.values()));
                return subcategories;
            }
            case SEMICONDUCTOR_COMPONENTS: {
                ArrayList<SubcategoriesEnum> subcategories = new ArrayList<>();
                subcategories.addAll(Arrays.asList(SemiconductorsCategoriesENUM.values()));
                return subcategories;
            }
            default: {
                ArrayList<SubcategoriesEnum> subcategories = new ArrayList<>();
                subcategories.addAll(Arrays.asList(SemiconductorsCategoriesENUM.values()));
                return subcategories;
            }
        }
    }

    /**
     * Metoda dostarcza ENUM z listy subcategories, wybrany na podstawie
     * etykiety subcategoryLabel.
     *
     * @param subcategories lista wszystkich podkategorii.
     * @param subcategoryLabel etykieta z wybraną podkategorią.
     * @return ENUM reprezentujący podkategorię.
     */
    @Override
    public SubcategoriesEnum getSubcategoryEnumFromLabel(List<SubcategoriesEnum> subcategories, String subcategoryLabel) {
        for (SubcategoriesEnum subcategory : subcategories) {
            if (subcategoryLabel.equals(subcategory.getLabel())) {
                return subcategory;
            }
        }
        return null;
    }

    /**
     * Metoda dodaje do modelu podkategorie z liczbą dostępnych elementów.
     *
     * @param model 'pojemnik' na obiekty przekazywane do widoku.
     */
    @Override
    public void putToModelSubcategoriesWithElementsAmount(Model model) {
        model.addAttribute("passiveSubcategories", getSubcategoriesWithNumberOfElements(MainCategoryENUM.PASSIVE_COMPONENTS));
        model.addAttribute("semiconductorsSubcategories", getSubcategoriesWithNumberOfElements(MainCategoryENUM.SEMICONDUCTOR_COMPONENTS));

//        Pozostałe kategorie do ew. późniejszego wykorzystania
//        model.addAttribute("connectorSubcategories", getSubcategoriesWithNumberOfElements(MainCategoryENUM.CONNECTOR_COMPONENTS));
//        model.addAttribute("optoelectronicsAndLightSubcategories", getSubcategoriesWithNumberOfElements(MainCategoryENUM.OPTOELECTRONICS_AND_LIGHT_COMPONENTS));
//        model.addAttribute("enclosures", getSubcategoriesWithNumberOfElements(MainCategoryENUM.ENCLOSURES));
//        model.addAttribute("fans", getSubcategoriesWithNumberOfElements(MainCategoryENUM.FANS));
//        model.addAttribute("fusesAndCircuitBreakers", getSubcategoriesWithNumberOfElements(MainCategoryENUM.FUSES_AND_CURCUIT_BREAKERS));
//        model.addAttribute("others", getSubcategoriesWithNumberOfElements(MainCategoryENUM.OTHERS));
//        model.addAttribute("powerSources", getSubcategoriesWithNumberOfElements(MainCategoryENUM.POWER_SOURCES));
//        model.addAttribute("relaysAndContactrons", getSubcategoriesWithNumberOfElements(MainCategoryENUM.RELAYS_AND_CONTACTRONS));
//        model.addAttribute("switchesAndIndicators", getSubcategoriesWithNumberOfElements(MainCategoryENUM.SWITCHES_AND_INDICATORS));
//        model.addAttribute("transformersAndFerriteCores", getSubcategoriesWithNumberOfElements(MainCategoryENUM.TRANSFORMERS_AND_FERRITE_CORES));
//        model.addAttribute("soundSources", getSubcategoriesWithNumberOfElements(MainCategoryENUM.SOUND_SOURCES));
    }

    @Override
    public void deleteComponent(int id, Class clazz) {
        if (clazz == null) {
            throw new NoSubcategoryFoundException();
        }
        Component component = getComponentById(id, clazz);
        if (component == null) {
            throw new UnknownComponentIdException();
        }
        componentRepository.deleteComponent(component);
    }
}
