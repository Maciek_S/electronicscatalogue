<%-- 
    Document   : resistors
    Created on : 2016-06-05, 08:31:00
    Author     : Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@taglib prefix="form" uri= "http://www.springframework.org/tags/form" %>
<%@taglib prefix="spring" uri= "http://www.springframework.org/tags" %>

<c:set var="selectedMainCategory" value='${passiveSubcategories}'/>
<div id="selectedMainCategory" style="display: none;">resistors</div>
<section>
    <div class="row">
        <form:form modelAttribute="componentCategory" class="form-horizontal">
            <fieldset>
                <legend>Wybierz kategorię</legend>
                <div class="form-group">
                    <label class="form-label col-lg-4" for="mainCategory">
                        <spring:message code="categorySelection.form.mainCategory.label"/>
                    </label>
                    <div class="col-lg-8">
                        <form:select id="mainCategory" path="mainCategory" name="mainCategory">
                            <c:forEach items="${mainCategories}" var="category">
                                <form:option value="${category}" label="${category.label}"/>
                            </c:forEach>
                        </form:select> 
                    </div>
                </div>
                <p  style="display: none;"></p>
                <div class="form-group subcategory">
                    <label class="form-label col-lg-4" for="subcategory">
                        <spring:message code="categorySelection.form.subcategory.label"/>
                    </label>
                    <div class="col-lg-8">
                        <form:select id="subcategory" path="subcategory" name="subcategory" title="Wybierz podkategorię">
                            <form:option value="Wybierz podkategorię"/>
                        </form:select> 
                        <img src="${pageContext.request.contextPath}/img/ajax.gif" class="ajax hidden" alt="loading..."/>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-offset-6 col-lg-4">
                        <button type="submit" id="btnAdd" class="btn btn-primary" value="Dodaj" disabled>
                            Dalej <span class="glyphicon glyphicon-hand-right"></span> 
                        </button>
                    </div>
                </div>
            </fieldset>
        </form:form>
    </div>
</section>