<%-- 
    Document   : componentParameterForm
    Created on : 2016-08-27, 00:16:00
    Author     : Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
--%>

<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<section class="container" style="text-align: center">
    <h3>Komponent został pomyślnie dodany do bazy danych.</h3>
    <a href="<spring:url value="/addNewComponent/categorySelection" />" class="btn btn-success">
       Dodaj następny komponent <span class="glyphicon-hand-right glyphicon"></span> 
    </a>
</section>