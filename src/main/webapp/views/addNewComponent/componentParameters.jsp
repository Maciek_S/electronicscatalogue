<%-- 
    Document   : componentParameterForm
    Created on : 2016-06-29, 19:52:17
    Author     : Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
--%>

<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@taglib prefix="form" uri= "http://www.springframework.org/tags/form" %>
<%@taglib prefix="spring" uri= "http://www.springframework.org/tags" %>

<section>
    <p>Kategoria: <span style="font-weight: bold">${selectedMainCategory.label} / ${selectedSubcategory.label}</span></p>
    <form:form modelAttribute="component" class="form-horizontal">
        <fieldset>
            <legend>Uzupełnij parametry komponentu</legend>
            <c:forEach items="${fields}" var="field">
                <c:if test="${field != 'id' and field != 'label'and field != 'mainCategory'and field != 'subcategory' and field !='description'}">
                    <div class="form-group">
                        <label class="form-label col-lg-4" for="${field}">

                            <c:if test="${field != 'unitsInStock' and field != 'tolerance' and field != 'nominalValueUnit' and field !='nominalValue' and field != 'dimensions'}">
                                <spring:message code="componentParameters.form.${componentClassName}.${field}"/> 
                            </c:if>
                            <c:if test="${field == 'unitsInStock' or field == 'tolerance' or field == 'nominalValueUnit' or field == 'nominalValue' or field == 'dimensions'}">
                                <spring:message code="componentParameters.form.all.${field}"/>
                            </c:if>
                        </label>
                        <div class="col-lg-8">
                            <c:choose>
                                <c:when test="${field == 'assembly'}">
                                    <form:select id="${field}" path="${field}" name="${field}">
                                        <c:forEach var="item" items="${assemblingTechnologies}">
                                            <form:option label="${item}" value="${item}"/>
                                        </c:forEach> 
                                    </form:select>
                                    <form:errors path="${field}" cssClass="text-danger"/>
                                </c:when>
                                <c:when test="${field == 'nominalValueUnit'}">
                                    <form:select id="nominalValueUnit" path="nominalValueUnit" name="nominalValueUnit">
                                        <c:forEach var="item" items="${nominalValueUnits}">
                                            <form:option label="${item.label}" value="${item}" htmlEscape="false"/>
                                        </c:forEach>
                                    </form:select>
                                    <form:errors path="nominalValueUnit" cssClass="text-danger"/>
                                </c:when>
                                <c:when test="${field == 'diodeType'}">
                                    <form:select id="${field}" path="${field}" name="${field}">
                                        <c:forEach var="item" items="${diodeTypes}">
                                            <form:option label="${item.label}" value="${item}"/>
                                        </c:forEach> 
                                    </form:select>
                                    <form:errors path="${field}" cssClass="text-danger"/>
                                </c:when>
                                <c:when test="${field == 'icType'}">
                                    <form:select id="${field}" path="${field}" name="${field}">
                                        <c:forEach var="item" items="${icTypes}">
                                            <form:option label="${item.label}" value="${item}"/>
                                        </c:forEach> 
                                    </form:select>
                                    <form:errors path="${field}" cssClass="text-danger"/>
                                </c:when>
                                <c:when test="${field == 'icCaseType'}">
                                    <form:select id="${field}" path="${field}" name="${field}">
                                        <c:forEach var="item" items="${icCaseTypes}">
                                            <form:option label="${item.label}" value="${item}"/>
                                        </c:forEach> 
                                    </form:select>
                                    <form:errors path="${field}" cssClass="text-danger"/>
                                </c:when>
                                <c:when test="${field == 'transistorType'}">
                                    <form:select id="${field}" path="${field}" name="${field}">
                                        <c:forEach var="item" items="${transistorTypes}">
                                            <form:option label="${item.label}" value="${item}"/>
                                        </c:forEach> 
                                    </form:select>
                                    <form:errors path="${field}" cssClass="text-danger"/>
                                </c:when>
                                <c:when test="${field == 'transistorCaseType'}">
                                    <form:select id="${field}" path="${field}" name="${field}">
                                        <c:forEach var="item" items="${transistorCaseTypes}">
                                            <form:option label="${item.label}" value="${item}"/>
                                        </c:forEach> 
                                    </form:select>
                                    <form:errors path="${field}" cssClass="text-danger"/>
                                </c:when>
                                <c:otherwise>
                                    <form:input id="${field}" path="${field}" name="${field}"/>
                                    <form:errors path="${field}" cssClass="text-danger"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                    <p  style="display: none;"></p>
                </c:if>
                <c:if test="${field == 'subcategory' or field == 'mainCategory'}">
                    <div class="form-group">
                        <form:input id="${field}" path="${field}" name="${field}" type="hidden"/>
                    </div>
                </c:if>
            </c:forEach>

            <div class="form-group">
                <div class="col-lg-offset-6 col-lg-4">
                    <button type="submit" id="btnAdd" class="btn btn-primary" value="Dodaj">
                        <span class="glyphicon glyphicon-plus-sign"></span> Dodaj
                    </button>
                </div>
            </div>
        </fieldset>
    </form:form>
</section>