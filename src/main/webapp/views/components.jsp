<%-- 
    Document   : resistors
    Created on : 2016-04-15, 21:18:14
    Author     : Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<section>
    <div class="row">

        <c:forEach items="${components}" var="component">
            <c:set var="componentSubcategory" value="${component.subcategory}"/>
            <c:set var="componentSubcategoryLowerCase" value="${fn:toLowerCase(componentSubcategory)}" />
            <div class="col-sm-6 col-md-6" style="padding-bottom: 15px">
                <div class="thumbnail thumbnailComponent">
                    <div class="deleteButton" data-id="${component.id}">
                        <a onclick="if (confirm('Czy na pewno skasować?')) {
                                    ajaxDeleteComponent('${componentSubcategoryLowerCase}/deleteComponent', ${component.id});
                                }" class="link">
                            <img src="${pageContext.request.contextPath}/img/ico/button_close.png" class="deleteComponentImg" alt="Kasuj"/>
                        </a>
                    </div>
                    <div class="moreDiv">
                        <a href="${componentSubcategoryLowerCase}/componentDetails?id=${component.id}">
                            <img src="${pageContext.request.contextPath}/img/ico/button_component_details.png" class="componentDetailsImg" alt="Szczegóły komponentu"/>
                        </a>
                    </div>
                    <div class="caption">
                        <h3 class="text-right">${component.label}</h3>
                        <p class="text-right">${component.description}</p>
                        <p class="unitsInStock text-right" data-componentid="${component.id}">Liczba sztuk w magazynie: ${component.unitsInStock}</p>
                        <div style="text-align: right;">

                            <div class="hidden">${componentSubcategoryLowerCase}</div>
                            <form data-componentid="${component.id}" data-unitsinstock="${component.unitsInStock}" class="plusMinusForm">
                                <div>
                                    <a onclick="ajaxChangeComponentAmount('${componentSubcategoryLowerCase}/take', ${component.id}, 1)" class="link" data-type="minus">
                                        <img src="${pageContext.request.contextPath}/img/ico/button_minus.ico" alt="Pobierz z magazynu" class="takeComponentImg"/>
                                    </a>
                                </div>
                                <div>
                                    <input class="value" type="text" style="width: 30px;" value="1"/>
                                </div>
                                <div>
                                    <a onclick="ajaxChangeComponentAmount('${componentSubcategoryLowerCase}/add', ${component.id}, 1)" class="link" data-type="plus">
                                        <img src="${pageContext.request.contextPath}/img/ico/button_plus.ico" alt="Zwróć do magazynu" class="returnComponentImg"/>
                                    </a> 
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </c:forEach>
        <div class="nextPrev col-lg-12">
            <c:if test="${page != 1}">
                <a href="${componentSubcategoryLowerCase}?page=${page-1}" class="linkNextPrev">Poprzednie</a>
            </c:if>

            <c:forEach begin="1" end="${pageTotal}" var="i">
                <c:choose>
                    <c:when test="${page eq i}">
                        <span class="currentPage">${i}</span>
                    </c:when>
                    <c:otherwise>
                        <a href="${componentSubcategoryLowerCase}?page=${i}" class="linkNextPrev">${i}</a>
                    </c:otherwise>
                </c:choose>
            </c:forEach>

            <c:if test="${page lt pageTotal}">
                <a href="${componentSubcategoryLowerCase}?page=${page + 1}" class="linkNextPrev">Następne</a>
            </c:if>
        </div>
    </div>

</section>