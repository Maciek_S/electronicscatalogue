<%-- 
    Document   : componentDetails
    Created on : 2017-01-06, 17:48:00
    Author     : Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<section>
    <div class="row">
        <div id="category">   
            <p>Kategoria: 
                <span style="font-weight: bold">${component.mainCategory.label} / ${component.subcategory.label}</span>
            </p>
        </div>
        <div id="form">
            <form:form modelAttribute="component" class="form-horizontal">
                <c:forEach items="${fields}" var="field" varStatus="count">
                    <c:if test="${field != 'id' and field != 'label'and field != 'mainCategory'and field != 'subcategory' and field !='description' and field != 'nominalValueUnit'}">
                        <div class="form-group">

                            <label class="form-label col-lg-4" for="${field}">
                                <c:if test="${field != 'unitsInStock' and field != 'tolerance' and field !='nominalValue' and field != 'dimensions'}">
                                    <spring:message code="componentParameters.form.${componentClassName}.${field}"/> 
                                </c:if>
                                <c:if test="${field == 'unitsInStock' or field == 'tolerance' or field == 'nominalValue' or field == 'dimensions'}">
                                    <spring:message code="componentParameters.form.all.${field}"/>
                                </c:if>
                            </label>
                            <div class="col-lg-8">
                                <c:choose>
                                    <c:when test="${field == 'nominalValue'}">
                                        <c:set var="paramName">${field}</c:set>
                                        ${component[paramName]} ${component.nominalValueUnit.label}
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="paramName">${field}</c:set>
                                        ${component[paramName]}
                                    </c:otherwise>
                                </c:choose>

                            </div>

                        </div>

                    </c:if>
                </c:forEach>         
            </form:form>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-4 col-lg-8">
                <a onclick="goBack()" class="btn btn-success">
                    <span class="glyphicon-hand-left glyphicon"></span> Wstecz
                </a>
            </div>
        </div>
    </div>
</section>