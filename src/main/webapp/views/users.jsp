<%-- 
    Document   : userProfile
    Created on : 2016-12-07, 10:00:00
    Author     : Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
--%>

<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<h4>Lista użytkowników</h4>
<section>
    <table class="table table-hover">
        <tr>
            <th>Login</th>
            <th>Imię i nazwisko</th>
            <th>E-mail</th>
            <th>Telefon</th>
            <th>Uprawnienia</th>
            <th>Status</th>
        </tr>

        <c:forEach items="${users}" var="user">
            <tr class="userTriger" data-usrid="${user.username}">
                <td>${user.username}</td>
                <td>${user.name} ${user.surname} </td>
                <td>${user.email}</td>
                <td>${user.mobile}</td>


                <td>
                    <c:set var="roleList" value="${user.userRoles}"/>
                    <c:forEach items="${roleList}" var="role" varStatus="counter">
                        <c:if test="${role.role.label == 'USER'}">
                            <img src="${pageContext.request.contextPath}/img/ico/role_user_active.png" alt="Użytkownik" class="ico">
                        </c:if>
                        <c:if test="${role.role.label == 'ADMIN'}">
                            <img src="${pageContext.request.contextPath}/img/ico/role_admin_active.png" alt="Administrator" class="ico">
                        </c:if>
                    </c:forEach>
                </td>
                <td>
                    <c:choose>
                        <c:when test="${user.enabled == 1}">
                            <img src="${pageContext.request.contextPath}/img/ico/active.png" alt="Aktywny" class="ico">
                        </c:when>
                        <c:otherwise>
                            <img src="${pageContext.request.contextPath}/img/ico/lock.png" alt="Blokada" class="ico">
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
        </c:forEach> 
    </table>


</section>
