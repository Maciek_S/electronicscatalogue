<%-- 
    Document   : userProfile
    Created on : 2016-09-06, 21:29:49
    Author     : Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
--%>

<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<h4>Profil użytkownika ${user.username}</h4>
<section>
    <div class="row">
        <div>Imię: ${user.name}</div>
        <div>Nazwisko: ${user.surname}</div>
        <div>E-mail: ${user.email}</div>
        <div>Telefon: ${user.mobile}</div>
        <div>Przyznane role:
            <c:set var="roleList" value="${user.userRoles}"/>
            <c:forEach items="${roleList}" var="role" varStatus="counter">
                <c:if test="${role.role.label == 'USER'}">
                    <img src="${pageContext.request.contextPath}/img/ico/role_user_active.png" alt="Użytkownik" class="ico">
                </c:if>
                <c:if test="${role.role.label == 'ADMIN'}">
                    <img src="${pageContext.request.contextPath}/img/ico/role_admin_active.png" alt="Administrator" class="ico">
                </c:if>
            </c:forEach>
        </div>
        <div>Ostatnia zmiana hasła: 
            <c:if test="${not empty user.datepasschange}">
                ${user.datepasschange}
            </c:if>
            <c:if test="${empty user.datepasschange}">
                Brak danych
            </c:if>
        </div>
        <div>
            <br/>
            <a href="<spring:url value="/users/userProfileForm?login=${user.username}"/>" class="link">
                <img src="${pageContext.request.contextPath}/img/ico/edit.ico" alt="Edytuj profil" class="ico"/>
                Edycja profilu 
            </a><br/>
            <a href="<spring:url value="/users/changePasswordForm?login=${user.username}"/>" class="link"> 
                <img src="${pageContext.request.contextPath}/img/ico/password.ico" alt="Zmień hasło" class="ico"/>
                Zmiana hasła
            </a> 

        </div>
    </div>
</section>
