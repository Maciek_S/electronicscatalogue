<%-- 
    Document   : emptyCategory
    Created on : 2016-09-11, 22:30:21
    Author     : Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
--%>


<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" isErrorPage="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<section class="container" style="text-align: center">
    <h3>${exceptionMessage}</h3>
    <a href="/ElectronicsCatalogue" onclick="goBack()" class="btn btn-danger">
        <span class="glyphicon-hand-left glyphicon"></span> Powrót</a>
</section>