<%-- 
    Document   : userProfile
    Created on : 2016-09-06, 21:29:49
    Author     : Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
--%>

<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri= "http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<section>
    <!-- Nagłówek -->
    <c:choose>
        <c:when test="${action == 'edit'}">
            <h4>Edycja profilu użytkownika ${user.username}</h4>
        </c:when>
        <c:otherwise>
            <h4>Tworzenie profilu nowego użytkownika</h4>
        </c:otherwise>
    </c:choose>

    <!-- Forma edycji profilu użytkownika (bez zmiany hasła) -->
    <form:form modelAttribute="user" class="form-horizontal" enctype="multipart/form-data">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <c:choose>
            <c:when test="${action == 'edit'}">
                <div class="form-group" style="display: none;">
                    <label class="control-label col-lg-4" for="username">
                        <spring:message code="userProfile.form.username"/>
                    </label>
                    <div class="col-lg-8">
                        <form:input id="username" path="username" name="username"/>
                        <form:errors path="username" cssClass="text-danger"/>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <c:set var="admin_img_type" value="inactive"/>
                <c:set var="user_img_type" value="inactive"/>
                <div class="form-group">
                    <label class="control-label col-lg-4" for="username">
                        <spring:message code="userProfile.form.username"/>
                    </label>
                    <div class="col-lg-8">
                        <form:input id="username" path="username" name="username"/>
                        <form:errors path="username" cssClass="text-danger"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4" for="password">
                        <spring:message code="userProfile.form.password"/>
                    </label>
                    <div class="col-lg-8">
                        <form:password id="password" path="password" name="password"/>
                        <form:errors path="password" cssClass="text-danger"/>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>
        <div class="form-group">
            <label class="control-label col-lg-4" for="name">
                <spring:message code="userProfile.form.name"/>
            </label>
            <div class="col-lg-8">
                <form:input id="name" path="name" name="name"/>
                <form:errors path="name" cssClass="text-danger"/>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-lg-4" for="surname">
                <spring:message code="userProfile.form.surname"/>
            </label>
            <div class="col-lg-8">
                <form:input id="surname" path="surname" name="surname"/>
                <form:errors path="surname" cssClass="text-danger"/>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-lg-4" for="mobile">
                <spring:message code="userProfile.form.mobile"/>
            </label>
            <div class="col-lg-8">
                <form:input id="mobile" path="mobile" name="mobile"/>
                <form:errors path="mobile" cssClass="text-danger"/>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-lg-4" for="email">
                <spring:message code="userProfile.form.email"/>
            </label>
            <div class="col-lg-8">
                <form:input id="email" path="email" name="email"/>
                <form:errors path="email" cssClass="text-danger"/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-4 col-lg-8">
                <a href="/ElectronicsCatalogue/users/userProfile?login=${user.username}" class="btn btn-info">
                    <span class="glyphicon-hand-left glyphicon"></span> Anuluj
                </a>
                <input type="submit" id="btnAdd" class="btn btn-primary" value ="Zapisz"/>

            </div>
        </div>
    </form:form>

    <!-- Zmiana ról użytkownika / blokowanie konta -->
    <div id="roles">
        <c:if test="${action == 'edit'}">
            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <h4>Zmiana przydzielonych uprawnień</h4>
                <c:set var="roleList" value="${user.userRoles}"/>
                <c:set var="isUserSet" value="false"/>
                <c:set var="isAdminSet" value="false"/>
                <c:if test='${empty roleList}'>
                    <c:set var="user_img_type" value="inactive"/>
                    <c:set var="admin_img_type" value="inactive"/>
                </c:if>
                <c:forEach items="${roleList}" var="role" varStatus="counter">
                    <c:choose>
                        <c:when test="${fn:contains(role.role.label, 'USER')}">
                            <c:set var="user_img_type" value="active"/>
                            <c:set var="isUserSet" value="true"/>
                        </c:when>
                        <c:otherwise>
                            <c:if test="${isUserSet == 'false'}">
                                <c:set var="user_img_type" value="inactive"/>
                            </c:if>
                        </c:otherwise>
                    </c:choose>
                    <c:choose>
                        <c:when test="${fn:contains(role.role.label, 'ADMIN')}">
                            <c:set var="admin_img_type" value="active"/>
                            <c:set var="isAdminSet" value="true"/>
                        </c:when>
                        <c:otherwise>
                            <c:if test="${isAdminSet == 'false'}">
                                <c:set var="admin_img_type" value="inactive"/>
                            </c:if>
                        </c:otherwise>
                    </c:choose>

                </c:forEach>


                <c:choose>
                    <c:when test="${user.enabled == 1}">
                        <c:set var="lock_img" value="lock_inactive.png"/>
                        <c:set var="lock_type" value="true"/>
                        <c:set var="message" value="Czy na pewno chcesz zablokować użytkownika?"/>
                    </c:when>
                    <c:otherwise>
                        <c:set var="lock_img" value="lock_active.png"/>
                        <c:set var="lock_type" value="false"/>
                        <c:set var="message" value="Czy na pewno chcesz odblokować użytkownika?"/>
                    </c:otherwise>
                </c:choose>

                <div id="userRoles" data-username="${user.username}">
                    <img src="${pageContext.request.contextPath}/img/ico/role_user_${user_img_type}.png" alt="Użytkownik" id="userLink" data-type="${user_img_type}"/>
                    <img src="${pageContext.request.contextPath}/img/ico/role_admin_${admin_img_type}.png" alt="Administrator" id="adminLink" data-type="${admin_img_type}"/>   

                    <a onclick="if (confirm('${message}')) {
                                ajaxLockUser('lockUser', '${user.username}', ${lock_type});
                            }" class="link">
                        <img src="${pageContext.request.contextPath}/img/ico/${lock_img}" id="lockLink" alt="Blokada" data-enabled="${user.enabled}">
                    </a>
                </div>
            </sec:authorize>
        </c:if>
    </div>
</section>