<%-- 
    Document   : welcome
    Created on : 2016-04-15, 20:35:01
    Author     : Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Katalog elementów elektronicznych</title>
    </head>
    <body>
        <section>
            <div class="jumbotron">
                <div class="container">
                    <h1> Katalog elementów elektronicznych </h1>
                </div>
            </div>
        </section>
        <div>
            <a href="components/passive_components/resistors">Rezystory</a><br/>
            <a href="users/userList">Użytkownicy</a><br/>
    </body>
</html>
