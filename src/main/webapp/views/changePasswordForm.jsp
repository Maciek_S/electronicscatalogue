<%-- 
    Document   : userProfile
    Created on : 2017-01-08, 15:59:00
    Author     : Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
--%>

<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri= "http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<section>
    <h4>Zmiana hasła użytkownika ${userPassword.username}</h4>
    <form:form modelAttribute="userPassword" class="form-horizontal" enctype="multipart/form-data">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <form:hidden id="username" path="username" name="username" />
        <div class="form-group">
            <label class="control-label col-lg-4" for="password">
                <spring:message code="userProfile.form.oldPassword"/>
            </label>
            <div class="col-lg-8">
                <form:password id="oldPassword" path="oldPassword" name="oldPassword"/>
                <form:errors path="oldPassword" cssClass="text-danger"/>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-lg-4" for="password">
                <spring:message code="userProfile.form.password"/>
            </label>
            <div class="col-lg-8">
                <form:password id="newPassword" path="newPassword" name="newPassword"/>
                <form:errors path="newPassword" cssClass="text-danger"/>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-lg-4" for="repetedPassword">
                <spring:message code="userProfile.form.repetedPassword"/>
            </label>
            <div class="col-lg-8">
                <form:password id="repetedPassword" path="repetedPassword" name="repetedPassword"/>
                <form:errors path="repetedPassword" cssClass="text-danger"/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-4 col-lg-8">
                <a href="/ElectronicsCatalogue/users/userProfile?login=${userPassword.username}" class="btn btn-info">
                    <span class="glyphicon-hand-left glyphicon"></span> Anuluj
                </a>
                <input type="submit" id="btnAdd" class="btn btn-primary" value ="Zapisz"/>
            </div>
        </div>
    </form:form>
</section>