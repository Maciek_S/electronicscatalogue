$(document).ready(function () {

    $('.userTriger').click(function () {
        var $this = $(this);
        var loginTriggered = $this.data('usrid');

        window.location.href = "userProfile?login=" + loginTriggered;
        return false;
    }); // koniec $('.userTriger').click(function () {

    // dodaj OnHover do ikon ról
    changeImgRoleSrcOnHover("userLink", "png");
    changeImgRoleSrcOnHover("adminLink", "png");

    // dodaj OnClick do ikon ról
    addRoleImgOnClickEvent("#userLink");
    addRoleImgOnClickEvent("#adminLink");

    // dodaj OnHover do ikony blokady użytkownika
    addOnHoverToLockLink();

}); // koniec funkcji ready

/**
 * Funkcja dodaje zdarzenie OnClick dla elementu <img> o id=ImgId
 * @param {type} imgId id elementu <img>
 * @returns {undefined}
 */
function addRoleImgOnClickEvent(imgId) {
    var imgElement = $(imgId);
    var imgData = imgElement.data("type");

    var userRolesElement = $("#userRoles");
    var username = userRolesElement.data("username");

    var link = "";
    if (imgData === 'active') {
        link = "removeRole?login=" + username + "&roleName=" + getRoleStringFromImgId(imgId);

    } else if (imgData === 'inactive') {
        link = "addRole?login=" + username + "&roleName=" + getRoleStringFromImgId(imgId);
    }
    imgElement.click(function () {
        window.location.href = link;
    });
}

/**
 * Funkcja na podstawie id elementu <img> zawierającego ikonę roli, 
 * zwraca ciąg znaków reprezentujących rolę.
 * @param {type} imgId id elementu <img> zawierającego ikonę roli
 * @returns {String} "ROLE_USER" lub "ROLE_ADMIN"
 */
function getRoleStringFromImgId(imgId) {
    if (imgId === "#userLink") {
        return "ROLE_USER";
    } else if (imgId === "#adminLink") {
        return "ROLE_ADMIN";
    }
}
/**
 * Funkcja zamienia ikonę symbolizującą rolę użytkownika po najechaniu myszą.
 * Kolor pierwotny symbolizuje stan roli, po najechaniu użytkownik widzi stan
 * po ewentualnej zmianie.
 * @param {type} imgId id elementu <img>
 * @param {type} extension rozszerzenie pliku atrybutu src
 * @returns {undefined}
 */
function changeImgRoleSrcOnHover(imgId, extension) {
    var img = $("#" + imgId);
    var src = img.attr('src');
    var data = img.data("type");

    if (data === 'active') {
        img.mouseover(function () {
            img.attr('src', src.replace('_active', '_inactive'));
        });
        img.mouseout(function () {
            img.attr('src', src.replace('_inactive', '_active'));
        });
    } else {
        img.mouseover(function () {
            img.attr('src', src.replace('_inactive', '_active'));
        });
        img.mouseout(function () {
            img.attr('src', src.replace('_active', '_inactive'));
        });
    }
}
/**
 * Funkcja zamienia ikonę symbolizującą blokadę użytkownika po najechaniu myszą.
 * Kolor zależy od stanu użytkownika - zablokowany czy nie.
 * @returns {undefined}
 */
function addOnHoverToLockLink() {
    var img = $("#lockLink");
    var src = img.attr('src');
    var data = img.data("enabled");
    if (data === 0) {
        img.mouseover(function () {
            img.attr('src', src.replace('_active', '_inactive'));
        });
        img.mouseout(function () {
            img.attr('src', src.replace('_inactive', '_active'));
        });
    } else {
        img.mouseover(function () {
            img.attr('src', src.replace('_inactive', '_active'));
        });
        img.mouseout(function () {
            img.attr('src', src.replace('_active', '_inactive'));
        });
    }
}

/**
 * Funkcja wywołuje asynchroniczne zapytanie do serwera w celu wywołania akcji
 * zablokowania użytkownika
 * @param {type} url ścieżka obsługi zapytania w UserController
 * @param {type} login login użytkownika
 * @param {type} lock czy użytkownik ma zostać zablokowany czy nie - true lub false
 * @returns {undefined}
 */
function ajaxLockUser(url, login, lock) {
    //dane do wysłania w asynchronicznym zapytaniu
    var data = {
        login: login,
        lock: lock
    };
    //wykonanie asynchronicznego zapytania GET
    $.get(url, data, handleAjaxResponseLockUser);
}

/**
 * Funkcja obsługuje odpowiedź od serwera wysłaną po pozytywnym zablokowaniu
 * lub odblokowaniu użytkownika. Funkcja przetwarza komunikat zawierający ciąg:
 * login(username);flaga_lock(true - zablokowany, false - niezablokowany) 
 * i odświeża widok, usuwając komponent z listy.
 * @param {type} response odpowiedź od serwera
 * @returns {undefined}
 */
function handleAjaxResponseLockUser(response) {
    // response: np. user1;true
    if (response !== "") {
        var state = response.split(";")[1];
        var img = $("#lockLink");
        var src = img.attr('src');

        if (state === "true") {
            img.attr('src', src.replace('_inactive', '_active'));
        } else {
            img.attr('src', src.replace('_active', '_inactive'));
        }
    }
}
