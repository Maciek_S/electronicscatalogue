$(document).ready(function () {
    // umozliwienie wyboru subkategorii w przypadku gdy chcemy wybrac pierwsza kategorie glowna (Elementy pasywne)
    var initSelectedText = $('#mainCategory').children(":selected").text();
    if (initSelectedText === "Elementy pasywne") {
        fillSubcategories(initSelectedText);
    }

    // zmiana comboBoxa z glowna kategoria, przeladowanie podkategorii
    $('#mainCategory').change(function () {
        $('#subcategory').empty();
        // Odczytaj nowa kategorie glowna
        var newMainCategory = $(this).children(":selected").text();
        fillSubcategories(newMainCategory);
        $('#btnAdd').attr("disabled", false);
    });
    
    // uaktywnia przycisk "dalej" dopiero gdy wybierze sie podkategorie
    $('#subcategory').change(function () {
        $('#btnAdd').attr("disabled", false);
    });
});

// Funkcja uzupełnia comboBox z podkategoriami na podstawie podanej w argumencie nazwy kategorii głównej
function fillSubcategories(mainCategory) {
    ajaxGetSubcategories(mainCategory);

    var saveField = $('#selectedMainCategory');
    saveField.text(mainCategory);
}

// Funkcja za pomocą AJAX'a odpytuje logikę biznesową o podkategorie wchodzące w sklad kategorii glownej
function ajaxGetSubcategories(newMainCategory) {
    //wyświetlenie "spinnera" wskazującego działanie AJAXa
    $('img.ajax').show();

    //adres strony, która odpowie na asynchroniczne zapytanie
    var url = "categorySelection/getCategory?mainCategoryLabel=" + newMainCategory;

    //dane do wysłania w asynchronicznym zapytaniu
    var data = {
        mainCategory: newMainCategory
    };

    //wykonanie asynchronicznego zapytania GET
    $.get(url, data, handleAjaxResponseGetSubcategories);
}

// Funkcja dekoduje odpowiedź uzyskaną z serwera i przygotowuje nowy widok (wypelnia comboBox).
function handleAjaxResponseGetSubcategories(response) {
    var $results = $('#subcategory');

    if (response !== "") {
        var array = JSON.parse(response);

        // dodanie opcji do comboboxa
        for (var i = 0; i < array.length; i++) {
            var option = document.createElement('option');
            option.value = array[i].label;
            option.innerHTML = array[i].label;
            $results.append(option);
        }

        //ukrycie "spinnera" wskazującego działanie AJAXa
        $('img.ajax').hide();
    }


}