$(document).ready(function () {

    // zmiana dla każdego pola tekstowego z przyrostem
    $(".value").each(function () {
        var inputElement = $(this);
        var formElement = inputElement.parent().parent();

        var id = formElement.data("componentid");

        var componentSubcategoryLowerCase = $("#componentSubcategoryLowerCase").text();

        // listener dla wszystkich pol tekstowych
        inputElement.change(function () {
            var unitsInStock = formElement.data("unitsinstock");

            // znajduje wszystkie linki
            formElement.find(".link").each(function () {
                var step = inputElement.val();
                var aElement = $(this);
                var type = aElement.data("type");

                // sprawdza typ linku
                if (type === "plus") {
                    aElement.attr("onclick", "ajaxChangeComponentAmount('" + componentSubcategoryLowerCase + "/add', " + id + "," + step + ")");
                } else if (type === "minus") {
                    aElement.attr("onclick", "ajaxChangeComponentAmount('" + componentSubcategoryLowerCase + "/take', " + id + "," + step + ")");
                }
            });
        });
    }); // $(".value").each(function () {

    // obsługa mouseover i mouse out na divach komponentu (zmiana koloru)
    changeComponentDivBackgroundColor("thumbnail");

    // obsługa mouseover i mouse out na obrazkach (zmiana src elementu img)
    changeImgSrcOnHover("componentDetailsImg", "png");
    changeImgSrcOnHover("deleteComponentImg", "png");
    changeImgSrcOnHover("takeComponentImg", "ico");
    changeImgSrcOnHover("returnComponentImg", "ico");
});

/**
 * Funkcja dodaje zdarzenie mouseover i mouseout na elemencie oznaczonym 
 * klasą divClass, które zmieniają kolor tła elementu.
 * @param {type} divClass klasa elementu
 * @returns {undefined}
 */
function changeComponentDivBackgroundColor(divClass) {
    $("." + divClass).each(function () {
        var $this = $(this);
        $this.mouseover(function () {
            $this.css({
                'background-color': '#e7e7e7'
            });
        });
        $this.mouseout(function () {
            $this.css({
                'background-color': '#f5f5f5'
            });
        });
    });
}
/**
 * Funkcja dodaje zdarzenie mouseover i mouseout na elemencie img oznaczonym 
 * klasą imgClass, które zmienia atrybut src.
 * @param {type} imgClass klasa elementu img
 * @param {type} extension rozszerzenie pliku graficznego
 * @returns {undefined}
 */
function changeImgSrcOnHover(imgClass, extension) {
    $("." + imgClass).each(function () {
        var img = $(this);
        var src = img.attr('src');
        img.mouseover(function () {
            img.attr('src', src.replace('.'+extension, '_hover.'+extension));
        });
        img.mouseout(function () {
            img.attr('src', src.replace('_hover.'+extension, '.'+extension));
        });
    });
}

/**
 * Funkcja wywołuje asynchroniczne zapytanie do serwera w celu wywołania akcji
 * skasowania komponentu z bazy danych
 * @param {type} url ścieżka obsługi zapytania w ComponentController
 * @param {type} id numer identyfikacyjny komponentu
 * @returns {undefined}
 */
function ajaxDeleteComponent(url, id) {
    //dane do wysłania w asynchronicznym zapytaniu
    var data = {
        id: id
    };
    //wykonanie asynchronicznego zapytania GET
    $.get(url, data, handleAjaxResponseDeleteComponent);
}

/**
 * Funkcja obsługuje odpowiedź od serwera wysłaną po pozytywnym wykasowaniu
 * komponentu z bazy danych. Funkcja przetwarza komunikat zawierający id
 * skasowanego komponentu i odświeża widok, usuwając komponent z listy.
 * @param {type} response odpowiedź od serwera
 * @returns {undefined}
 */
function handleAjaxResponseDeleteComponent(response) {
    if (response !== "") {
        var deletedId = response;
        $(".deleteButton").each(function () {
            var $this = $(this);
            currentId = $this.data("id");
            if (String(deletedId) === String(currentId)) {
                $this.parent().parent().remove();
            }
        });
    }

}