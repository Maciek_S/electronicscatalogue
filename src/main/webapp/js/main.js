$(document).ready(function () {
    var date = new Date();
    $("#yearFooter").text("Katalog elementów elektronicznych, " + date.getFullYear());
});

$('#accordion').collapse({
    hidden: false
});

function ajaxChangeComponentAmount(url, id, amount) {
    //dane do wysłania w asynchronicznym zapytaniu
    var data = {
        id: id,
        amount: amount
    };
    //wykonanie asynchronicznego zapytania GET
    $.get(url, data, handleAjaxResponse);
}

function goBack() {
    window.history.back();
}

function handleAjaxResponse(response) {

    if (response !== "") {
        var splitTab = response.split(";");
        var id = splitTab[0];
        var newAmount = splitTab[1];

        // dla każdego komponentu znajdz element z liczba sztuk w magazynie
        $(".caption").find(".unitsInStock").each(function () {
            var $this = $(this);
            var componentId = $this.data("componentid");

            if (parseInt(id) === parseInt(componentId)) {
                $this.text("Liczba sztuk w magazynie: " + newAmount);
            }

        });
    }
}