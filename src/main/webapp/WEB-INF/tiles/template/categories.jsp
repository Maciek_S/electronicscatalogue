<%-- 
    Document   : categories
    Created on : 2016-04-21, 22:01:47
    Author     : Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<div class="panel panel-default" role="tablist">
    <!--Półprzewodniki-->
    <div class="panel-heading" role="tab" id="infoBox1">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                <img src="<spring:url value="/img/categories/polprzewodniki.jpg"/>" class="categoryImg" alt="Półprzewodniki"/> Półprzewodniki
            </a>
        </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="infoBox1">
        <div class="panel-body">
            <ul class="list-group">
                <c:forEach items="${semiconductorsSubcategories}" var="subcategory">
                    <c:choose>
                        <c:when test="${subcategory.value eq 0}">
                            <li class="list-group-item list-group-item-danger">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/semiconductor_components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li class="list-group-item">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/semiconductor_components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>          
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
        </div>
    </div>
    <!--Optoelektronika i źródła światła
    <div class="panel-heading" role="tab" id="infoBox2">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
                <img src="<spring:url value="/img/categories/optoelektronika.jpg"/>" class="categoryImg" alt="Optoelektronika"/> Optoelektronika i źródła światła
            </a>
        </h4>
    </div>
    <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="infoBox2">
        <div class="panel-body">
            <ul class="list-group">
                <c:forEach items="${optoelectronicsAndLightSubcategories}" var="subcategory">
                    <c:choose>
                        <c:when test="${subcategory.value eq 0}">
                            <li class="list-group-item list-group-item-danger">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li class="list-group-item">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>          
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
        </div>
    </div>-->
    <!--Elementy pasywne-->
    <div class="panel-heading" role="tab" id="infoBox3">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
                <img src="<spring:url value="/img/categories/pasywne.jpg"/>" class="categoryImg" alt="Elementy pasywne"/> Elementy pasywne
            </a>
        </h4>
    </div>
    <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="infoBox3">
        <div class="panel-body">
            <ul class="list-group">
                <c:forEach items="${passiveSubcategories}" var="subcategory">
                    <c:choose>
                        <c:when test="${subcategory.value eq 0}">
                            <li class="list-group-item list-group-item-danger">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/passive_components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li class="list-group-item">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/passive_components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>          
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
        </div>
    </div>
    <!--Złącza
    <div class="panel-heading" role="tab" id="infoBox4">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="true" aria-controls="collapse4">
                <img src="<spring:url value="/img/categories/zlacza.jpg"/>" class="categoryImg" alt="Złącza"/> Złącza
            </a>
        </h4>
    </div>
    <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="infoBox4">
        <div class="panel-body">
            <ul class="list-group">
                <c:forEach items="${connectorSubcategories}" var="subcategory">
                    <c:choose>
                        <c:when test="${subcategory.value eq 0}">
                            <li class="list-group-item list-group-item-danger">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li class="list-group-item">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>          
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
        </div>
    </div>-->
    <!--Bezpieczniki i zabezpieczenia
    <div class="panel-heading" role="tab" id="infoBox5">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="true" aria-controls="collapse5">
                <img src="<spring:url value="/img/categories/bezpieczniki.jpg"/>" class="categoryImg" alt="Bezpieczniki i zabezpieczenie"/> Bezpieczniki i zabezpieczenia
            </a>
        </h4>
    </div>
    <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="infoBox5">
        <div class="panel-body">
           <ul class="list-group">
                <c:forEach items="${fusesAndCircuitBreakers}" var="subcategory">
                    <c:choose>
                        <c:when test="${subcategory.value eq 0}">
                            <li class="list-group-item list-group-item-danger">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li class="list-group-item">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>          
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
        </div>
    </div>-->
    <!--Przełączniki i kontrolki
    <div class="panel-heading" role="tab" id="infoBox6">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="true" aria-controls="collapse6">
                <img src="<spring:url value="/img/categories/przelaczniki.jpg"/>" class="categoryImg" alt="Przełączniki i kontrolki"/> Przełączniki i kontrolki
            </a>
        </h4>
    </div>
    <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="infoBox6">
        <div class="panel-body">
            <ul class="list-group">
                <c:forEach items="${switchesAndIndicators}" var="subcategory">
                    <c:choose>
                        <c:when test="${subcategory.value eq 0}">
                            <li class="list-group-item list-group-item-danger">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li class="list-group-item">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>          
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
        </div>
    </div>-->
    <!--Źródła dźwięku
    <div class="panel-heading" role="tab" id="infoBox7">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="true" aria-controls="collapse7">
                <img src="<spring:url value="/img/categories/dzwiek.jpg"/>" class="categoryImg" alt="Źródła dźwięku"/> Źródła dźwięku
            </a>
        </h4>
    </div>
    <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="infoBox7">
        <div class="panel-body">
            <ul class="list-group">
                <c:forEach items="${soundSources}" var="subcategory">
                    <c:choose>
                        <c:when test="${subcategory.value eq 0}">
                            <li class="list-group-item list-group-item-danger">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li class="list-group-item">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>          
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
        </div>
    </div>-->
    <!--Przekaźniki i styczniki
    <div class="panel-heading" role="tab" id="infoBox8">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="true" aria-controls="collapse8">
                <img src="<spring:url value="/img/categories/przekazniki.jpg"/>" class="categoryImg" alt="Przekaźniki i styczniki"/> Przekaźniki i styczniki
            </a>
        </h4>
    </div>
    <div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="infoBox8">
        <div class="panel-body">
            <ul class="list-group">
                <c:forEach items="${relaysAndContactrons}" var="subcategory">
                    <c:choose>
                        <c:when test="${subcategory.value eq 0}">
                            <li class="list-group-item list-group-item-danger">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li class="list-group-item">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>          
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
        </div>
    </div>-->
    <!--Transformatory i rdzenie
    <div class="panel-heading" role="tab" id="infoBox9">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse9" aria-expanded="true" aria-controls="collapse9">
                <img src="<spring:url value="/img/categories/transformatory.jpg"/>" class="categoryImg" alt="Transformatory i rdzenie"/> Transformatory i rdzenie
            </a>
        </h4>
    </div>
    <div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="infoBox9">
        <div class="panel-body">
            <ul class="list-group">
                <c:forEach items="${transformersAndFerriteCores}" var="subcategory">
                    <c:choose>
                        <c:when test="${subcategory.value eq 0}">
                            <li class="list-group-item list-group-item-danger">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li class="list-group-item">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>          
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
        </div>
    </div>-->
    <!--Wentylatory, sys. chłodzenia i grzania
    <div class="panel-heading" role="tab" id="infoBox10">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse10" aria-expanded="true" aria-controls="collapse10">
                <img src="<spring:url value="/img/categories/wentylatory.jpg"/>" class="categoryImg" alt="Wentylatory"/> Wentylatory
            </a>
        </h4>
    </div>
    <div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="infoBox10">
        <div class="panel-body">
            <ul class="list-group">
                <c:forEach items="${fans}" var="subcategory">
                    <c:choose>
                        <c:when test="${subcategory.value eq 0}">
                            <li class="list-group-item list-group-item-danger">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li class="list-group-item">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>          
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
        </div>
    </div>-->
    <!--Źródła energii
    <div class="panel-heading" role="tab" id="infoBox11">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse11" aria-expanded="true" aria-controls="collapse11">
                <img src="<spring:url value="/img/categories/baterie.jpg"/>" class="categoryImg" alt="Źródła energii"/> Źródła energii
            </a>
        </h4>
    </div>
    <div id="collapse11" class="panel-collapse collapse" role="tabpanel" aria-labelledby="infoBox11">
        <div class="panel-body">
            <ul class="list-group">
                <c:forEach items="${powerSources}" var="subcategory">
                    <c:choose>
                        <c:when test="${subcategory.value eq 0}">
                            <li class="list-group-item list-group-item-danger">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li class="list-group-item">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>          
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
        </div>
    </div>-->

    <!--Obudowy
    <div class="panel-heading" role="tab" id="infoBox12">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse12" aria-expanded="true" aria-controls="collapse12">
                <img src="<spring:url value="/img/categories/obudowy.jpg"/>" class="categoryImg" alt="Obudowy"/> Obudowy
            </a>
        </h4>
    </div>
    <div id="collapse12" class="panel-collapse collapse" role="tabpanel" aria-labelledby="infoBox12">
        <div class="panel-body">
           <ul class="list-group">
                <c:forEach items="${enclosures}" var="subcategory">
                    <c:choose>
                        <c:when test="${subcategory.value eq 0}">
                            <li class="list-group-item list-group-item-danger">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li class="list-group-item">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>          
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
        </div>
    </div>-->
    <!--Różne
        <div class="panel-heading" role="tab" id="infoBox13">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse13" aria-expanded="true" aria-controls="collapse13">
                <img src="<spring:url value="/img/categories/rozne.jpg"/>" class="categoryImg" alt="Różne"/> Różne
            </a>
        </h4>
    </div>
    <div id="collapse13" class="panel-collapse collapse" role="tabpanel" aria-labelledby="infoBox13">
        <div class="panel-body">
           <ul class="list-group">
                <c:forEach items="${others}" var="subcategory">
                    <c:choose>
                        <c:when test="${subcategory.value eq 0}">
                            <li class="list-group-item list-group-item-danger">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li class="list-group-item">
                                <c:set var="lowerCaseSubcategory" value="${fn:toLowerCase(subcategory.key)}"/>
                                <a href="<spring:url value="/components/${lowerCaseSubcategory}"/>">${subcategory.key.label} <span class="badge">${subcategory.value}</span></a>
                            </li>          
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
        </div>
    </div>-->
</div>