<%-- 
    Document   : navigation
    Created on : 2016-04-18, 21:59:01
    Author     : Maciej St?pniak &lt;maciek.stepniak@gmail.com&gt;
--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<ul class="nav navbar-nav navbar-left">
    <li>
        <a href="<spring:url value="/components/passive_components/resistors"/>">
            <span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> 
            Katalog elementów</a>
    </li>   
    <li>
        <a href="<spring:url value="/addNewComponent/categorySelection"/>">
            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
            Dodaj element</a>
    </li>

    <li class="disabled"><a href="#">Raporty</a></li> 
    <li class="disabled"><a href="#">Pomoc</a></li> 

</ul>


<ul class="nav navbar-nav navbar-left navbar-right">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            Profil 
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu">
            <li>
                <sec:authentication var="loggedUser" property="principal.username" />
                <a href="<spring:url value="/users/userProfile?login=${loggedUser}"/>">
                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span> 
                    Moje konto
                </a>
            </li>
            <li><a href="#">Pobrane elementy</a></li>
            <li role="separator" class="divider"></li>

            <li>
                <a href="<spring:url value="/logout"/>"> 
                    <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>
                    Wyloguj</a>
            </li>
        </ul>
    </li>
</ul>

<sec:authorize access="hasRole('ROLE_ADMIN')">
    <ul class="nav navbar-nav navbar-left navbar-right">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                Administracja
                <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="<spring:url value="/users/userList"/>">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> 
                        Użytkownicy
                    </a>
                </li>
                <li>
                    <a href="<spring:url value="/users/addUser"/>">
                        <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> 
                        Dodaj użytkownika
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</sec:authorize>