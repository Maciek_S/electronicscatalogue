<%-- 
    Document   : baseLayout
    Created on : 2016-04-18, 21:57:02
    Author     : Maciej Stępniak &lt;maciek.stepniak@gmail.com&gt;
--%>

<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <!-- wykorzystanie najnowszego możliwego silnika przeglądarki, jeżeli użytkownik
        korzysta z Internet Explorera-->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!--dopasowanie strony/aplikacji do szerokości ekranu urządzenia-->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css">

        <title>Katalog elementów elektronicznych</title>

        <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery-2.2.3.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/main.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/users.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/addNewComponent.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/components.js"></script>

    </head>
    <body>
        <div class="container">
            <header>
                <nav class="navbar navbar-default navbar-fixed-top">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed"
                                    data-toggle="collapse"
                                    data-target="#mainmenu"
                                    aria-expanded="false"
                                    id="nav-switch">
                                <span class="sr-only">Przełącznik nawigacji</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="${pageContext.request.contextPath}">Katalog elementów elektronicznych</a>
                        </div>

                        <div class="collapse navbar-collapse" id="mainmenu">
                            <tiles:insertAttribute name="navigation"/>
                        </div>

                    </div>
                </nav>
            </header>
            <div class="col-sm-12">
                <tiles:insertAttribute name="content" />
            </div>
        </div>
        <footer>
            <tiles:insertAttribute name="footer" />
        </footer>
    </body>
</html>